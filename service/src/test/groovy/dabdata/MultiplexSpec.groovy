package dabdata

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification;
import dabdata.Multiplex;
import dabdata.Party;

class MultiplexSpec extends Specification implements DomainUnitTest<Multiplex> {

    def setup() {
    }

    def cleanup() {
    }

    void "Test basic persistence mocking"() {
      setup:
        Party p = new Party(shortcode:'ROOT', name:'System').save()
        new Multiplex(name:'MP1', owner:p).save()
        new Multiplex(name:'MP2', owner:p).save()

      expect:
        Multiplex.count() == 2
    }


    void "Test name is mandatory"() {
      when:"We create a multiplex without a name"
        def mp = new Multiplex()

      then:"Validation should fail"
        !mp.validate(['name'])

      and:"Unique error code is set"
        mp.errors['name']?.code == 'nullable'

      and:"Save does not fire"
        !mp.save()

      and:"Record count is still 0"
        Multiplex.count() == 0
    }

}
