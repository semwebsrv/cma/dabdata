package dabdata

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification
import dabdata.BroadcastService
import dabdata.Party

class BroadcastServiceSpec extends Specification implements DomainUnitTest<BroadcastService> {

    def setup() {
    }

    def cleanup() {
    }

    void "Test basic persistence mocking"() {
      setup:
        Party p = new Party(shortcode:'ROOT', name:"System").save()
        new BroadcastService(name:'BS1', sid:'BS1', owner:p).save()
        new BroadcastService(name:'BS2', sid:'BS2', owner:p).save()

      expect:
        BroadcastService.count() == 2
    }


    void "Test name is mandatory"() {
      when:"We create a broadcast service without a name"
        def bs = new BroadcastService()

      then:"Validation should fail"
        !bs.validate(['name'])

      and:"Unique error code is set"
        bs.errors['name']?.code == 'nullable'
      
      and:"Save does not fire"
        !bs.save()

      and:"Record count is still 2"
        BroadcastService.count() == 0
    }
}
