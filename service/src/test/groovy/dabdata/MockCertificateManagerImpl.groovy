package dabdata

import java.security.Key;
import java.security.PublicKey
import java.security.KeyFactory
import java.security.spec.X509EncodedKeySpec
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import org.springframework.beans.factory.annotation.Autowired
import grails.core.GrailsApplication

import groovy.util.logging.Slf4j

/**
 * 
 */
@Slf4j
public class MockCertificateManagerImpl implements CertificateManager {

  private Map<String, Object> keys = new HashMap<String, Object>();

  @Autowired
  GrailsApplication grailsApplication

  public MockCertificateManagerImpl() {
    KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
    keyPairGenerator.initialize(2048);
    KeyPair keyPair = keyPairGenerator.generateKeyPair();
    PrivateKey privateKey = keyPair.getPrivate();
    PublicKey publicKey = keyPair.getPublic();
    keys.put('private', privateKey);
    keys.put('public', publicKey);
  }

  public PublicKey getPublicKey() {
    return keys.get('public');
  }

  public PrivateKey getPrivateKey() {
    return keys.get('private');
  }

}
