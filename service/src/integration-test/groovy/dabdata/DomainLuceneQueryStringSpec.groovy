package dabdata


import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import dabdata.Multiplex
import groovy.util.logging.Slf4j
import spock.lang.Stepwise
import dabdata.Multiplex

@Slf4j
@Integration
@Rollback
class DomainLuceneQueryStringSpec extends Specification {

    def setup() {
    }

    void setupData() {
      Party p = Party.findByShortcode('ROOT');
      def multiplex_data = [
        [ 'name' : 'Test Multiplex 1', 'ead' : 'EAD1', 'area' : 'Sheffield',  'block' : '012', 'frequency' : '1231', 'licenseNumber' : 'l23', 'homepage' : null, 'owner': p ],
        [ 'name' : 'Test A Widget',    'ead' : 'EAD2', 'area' : 'London',     'block' : '013', 'frequency' : '1232', 'licenseNumber' : 'l24', 'homepage' : null, 'owner': p ],
        [ 'name' : 'Elephant',         'ead' : 'EAD3', 'area' : 'Manchester', 'block' : '014', 'frequency' : '1233', 'licenseNumber' : 'l26', 'homepage' : null, 'owner': p ],
        [ 'name' : 'Obscure Name',     'ead' : 'EAD4', 'area' : 'Brighton',   'block' : '015', 'frequency' : '1234', 'licenseNumber' : 'l27', 'homepage' : null, 'owner': p ],
        [ 'name' : 'Other End Test',   'ead' : 'EAD5', 'area' : 'Liverpool',  'block' : '016', 'frequency' : '1235', 'licenseNumber' : 'l28', 'homepage' : null, 'owner': p ],
        [ 'name' : 'Other Wibble',     'ead' : 'EAD6', 'area' : 'Glasgow',    'block' : '017', 'frequency' : '1236', 'licenseNumber' : 'l29', 'homepage' : null, 'owner': p ],
        [ 'name' : '221 Some Name',    'ead' : 'EAD7', 'area' : 'Dublin',     'block' : '018', 'frequency' : '1237', 'licenseNumber' : 'l30', 'homepage' : null, 'owner': p ]
      ]

      multiplex_data.each { rec ->
        new Multiplex(rec).save(flush:true, failOnError:true)
      }
      // new Multiplex(name:"Test A Widget").save(flush:true, failOnError:true)
      // new Multiplex(name:"Elephant").save(flush:true, failOnError:true)
      // new Multiplex(name:"Obscure Name").save(flush:true, failOnError:true)
      // new Multiplex(name:"Other End Test").save(flush:true, failOnError:true)
      // new Multiplex(name:"Other Wibble").save(flush:true, failOnError:true)
      // new Multiplex(name:"221 Some Name").save(flush:true, failOnError:true)
    }

    def cleanup() {
    }

    void "Test Lucene Query of Domain Classes"() {
      given:
        setupData()

      expect:"We execute some searches"
        def all_records = Multiplex.list()
        log.debug("All records: ${all_records}");

        grails.orm.PagedResultList r2 = Multiplex.findAllByLuceneQueryString('Wibble',[max:10, offset:0])
        log.debug("Result : ${r2}");
        r2.totalCount == 1

        grails.orm.PagedResultList r3 = Multiplex.findAllByLuceneQueryString('Elephant',[max:10, offset:0])
        log.debug("Result : ${r3}");
        r3.totalCount == 1

        grails.orm.PagedResultList r4 = Multiplex.findAllByLuceneQueryString('Widget AND Test',[max:10, offset:0])
        log.debug("Result : ${r4}");
        r4.totalCount == 1

        grails.orm.PagedResultList r5 = Multiplex.findAllByLuceneQueryString('"Widget Test"',[max:10, offset:0])
        log.debug("Result : ${r5}");
        r5.totalCount == 0

        grails.orm.PagedResultList r6 = Multiplex.findAllByLuceneQueryString('name:Other',[max:10, offset:0])
        log.debug("Result : ${r6}");
        r6.totalCount == 2
    }
}
