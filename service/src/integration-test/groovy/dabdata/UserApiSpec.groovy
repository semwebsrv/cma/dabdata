package dabdata


import grails.testing.mixin.integration.Integration
import grails.testing.spock.OnceBefore
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import dabdata.MockCertificateManagerImpl
import org.springframework.beans.factory.annotation.Autowired
import groovy.util.logging.Slf4j
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.Claims;
import java.time.Duration;
import io.micronaut.http.client.DefaultHttpClient 
import io.micronaut.http.client.HttpClientConfiguration
import io.micronaut.http.client.DefaultHttpClientConfiguration

@Slf4j
@Integration
@Stepwise
class UserApiSpec extends Specification {

    @Shared
    @AutoCleanup
    HttpClient client

    @Shared
    Map<String,Map> stashed_data = [:]

    // grails-app/conf/spring/resources.groovy arranges for certificateManager to be MockCertificateManagerImpl from the test src tree
    // this lets us mock the certificate rather than calling out to keycloak to get a real user token and makes our testing more
    // independent
    @Autowired
    MockCertificateManagerImpl certificateManager

    @Shared
    String session_jwt = null;

    @OnceBefore
    void init() {
        String baseUrl = "http://localhost:$serverPort"
        // this.client  = HttpClient.create(new URL(baseUrl))
        HttpClientConfiguration configuration = new DefaultHttpClientConfiguration()
        configuration.setConnectTimeout(Duration.ofSeconds(300))
        configuration.setReadTimeout(Duration.ofSeconds(300))
        this.client  = new DefaultHttpClient(new URL(baseUrl), configuration);

        def private_key = certificateManager.getPrivateKey()
        Date expiration = new Date(System.currentTimeMillis()+60000);
        List<String> roles = ['USER','ADMIN','SOCIALAUTH','EDITOR']

        this.session_jwt = Jwts.builder()
            .setIssuer("me")
            .setSubject("admin")
            .setAudience("you")
            .setId(UUID.randomUUID().toString()) //just an example id
            .setIssuedAt(new Date()) // for example, now
            .claim('roles', roles)
            .claim('realm-access', roles)
            .claim('preferred_username', 'ian.ibbotson@semweb.co')
            .claim('email', 'ian.ibbotson@semweb.co')
            .setExpiration(expiration) //a java.util.Date
            .setNotBefore(new Date()) //a java.util.Date 
            .signWith(private_key)
            .compact()

    }

  String setupData() {

    String result = null;

    // We need these committed to the DB so the thread in the controller will pick them up
    Party.withNewTransaction { status ->
      def p = new Party(shortcode:'W_TWO', name:'WIBBLE TWO').save(flush:true, failOnError:true);
      def p3 = new Party(shortcode:'W_THREE', name:'WIBBLE THREE').save(flush:true, failOnError:true);
      def p4 = new Party(shortcode:'W_FOUR', name:'WIBBLE FOUR').save(flush:true, failOnError:true);

      def mp = new Multiplex(name:'TestMp2', owner: p, ead:'TestMp2').save(flush:true, failOnError:true);
      log.debug("Created multiplex: ${mp}");
      result = mp.id;

      [ 
        [ name:'BS001', owner: p ],
        [ name:'BS002', owner: p ],
        [ name:'BS003', owner: p3 ],
        [ name:'BS004', owner: p4 ],
      ].each { bs ->
        BroadcastService bs_inst = new BroadcastService(name:bs.name, owner:bs.owner).save(flush:true, failOnError:true);
        log.debug("Created broadcast service ${bs_inst}");
      }

      [
        [ name:'Trans01', owner: p ],
        [ name:'Trans02', owner: p ]
      ].each { t ->
        def nt = new Transmitter(name: t.name, owner: t.owner).save(flush:true, failOnError:true);
      }

      [ 
        [ owner:'/ROOT/W_TWO',   type:'%', res:'%', perm:'EDITOR', gtype:'dabdata.Party', grantee:'ian.ibbotson@semweb.co' ],
        [ owner:'/ROOT/W_THREE', type:'%', res:'%', perm:'EDITOR', gtype:'dabdata.Party', grantee:'ian.ibbotson@semweb.co' ],
        [ owner:'/ROOT/W_TWO',   type:'%', res:'%', perm:'EDITOR', gtype:'dabdata.Party', grantee:'fred@some.isp' ],
        [ owner:'/ROOT/W_THREE', type:'%', res:'%', perm:'EDITOR', gtype:'dabdata.Party', grantee:'fred@some.isp' ],
        [ owner:'/ROOT/W_TWO',   type:'%', res:'%', perm:'EDITOR', gtype:'dabdata.Party', grantee:'freda@some.isp' ],
      ].each { g ->
        def gr = new Grant(
                  grantResourceOwner:g.owner,
                  grantResourceType:g.type,
                  grantResourceId:g.res,
                  grantedPerm:g.perm,
                  granteeType:g.gtype,
                  granteeId:g.grantee).save(flush:true, failOnError:true);
        log.debug("Created grant ${g}");
      }

      log.debug("Grants: ${Grant.list()}");
      log.debug("Broadcast Services: ${BroadcastService.list()}");
    }

    return result;
  }


    void testHomePageData() {

      given:
        def mp_id = setupData();

      when:"We call the user home page data endpoint"
        HttpRequest access_request = HttpRequest.GET("/dabdata/user/getHomePageData")
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        HttpResponse<Map> user_home_response = client.toBlocking().exchange(access_request,Map)
        Map result = user_home_response?.body()
      then:
        user_home_response != null;
        log.debug("Got response to getHomePageData: ${user_home_response} / ${result}");
    }

}
