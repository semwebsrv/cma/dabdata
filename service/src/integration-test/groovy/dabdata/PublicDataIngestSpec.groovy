package dabdata


import grails.testing.mixin.integration.Integration
import grails.testing.spock.OnceBefore
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification
import dabdata.MockCertificateManagerImpl
import org.springframework.beans.factory.annotation.Autowired
import groovy.util.logging.Slf4j
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.Claims;
import java.time.Duration;
import io.micronaut.http.client.DefaultHttpClient 
import io.micronaut.http.client.HttpClientConfiguration
import io.micronaut.http.client.DefaultHttpClientConfiguration



@Slf4j
@Integration
class PublicDataIngestSpec extends Specification {

    @Shared
    @AutoCleanup
    HttpClient client

    // grails-app/conf/spring/resources.groovy arranges for certificateManager to be MockCertificateManagerImpl from the test src tree
    // this lets us mock the certificate rather than calling out to keycloak to get a real user token and makes our testing more
    // independent
    @Autowired
    MockCertificateManagerImpl certificateManager

    @Shared
    String session_jwt = null;

    @OnceBefore
    void init() {
        String baseUrl = "http://localhost:$serverPort"
        // this.client  = HttpClient.create(new URL(baseUrl))
        HttpClientConfiguration configuration = new DefaultHttpClientConfiguration()
        configuration.setConnectTimeout(Duration.ofSeconds(300))
        configuration.setReadTimeout(Duration.ofSeconds(300))
        this.client  = new DefaultHttpClient(new URL(baseUrl), configuration);

        def private_key = certificateManager.getPrivateKey()
        Date expiration = new Date(System.currentTimeMillis()+60000);
        List<String> roles = ['USER','ADMIN']

        this.session_jwt = Jwts.builder()
            .setIssuer("me")
            .setSubject("admin")
            .setAudience("you")
            .setId(UUID.randomUUID().toString()) //just an example id
            .setIssuedAt(new Date()) // for example, now
            .claim('roles', roles)
            .claim('realm-access', roles)
            .setExpiration(expiration) //a java.util.Date
            .setNotBefore(new Date()) //a java.util.Date 
            .signWith(private_key)
            .compact()

    }

    void testIngest() {
      when:"We call the ingest endpoint"
        HttpRequest load_request = HttpRequest.GET("/dabdata/loadDataFromUrl")
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        // load_request.getParameters().add('source','https://www.ofcom.org.uk/__data/assets/file/0018/91305/TxParamsDAB.csv');
        // lets reference the local file instead of pulling it every time
        load_request.getParameters().add('source','file:./src/test/resources/TxParamsDAB_sample.csv');
        HttpResponse<Map> load_response = client.toBlocking().exchange(load_request,Map)

      then:"We use that JWT to access the multiplex endpoint"
        Map result = load_response.body()
        log.debug("Got response to GET message: ${load_response} / ${result}");
        assert result.rows_processed == 15
        assert result.multiplexes_created == 10
        assert result.multiplexes_updated == 5
        assert result.transmitters_created == 15
        assert result.transmitters_updated == 0 // We only report an update for changed data
        assert result.multiplex_transmitters_created == 15
        assert result.multiplex_transmitters_updated == 0
        assert result.broadcast_services_created == 143
        assert result.broadcast_services_updated == 171
        assert result.bearers_created == 213
        assert result.bearers_updated == 101
        assert result.bearer_errors == 0
        assert result.errors == 0
        assert result.status == 'OK'

      then:"We search for a multiplex just created"
        HttpRequest qry_request = HttpRequest.GET("/dabdata/multiplex")
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        qry_request.getParameters().add('q','name:"Trial Woking"');
        qry_request.getParameters().add('setname','brief');
        HttpResponse<Map> qry_response = client.toBlocking().exchange(qry_request,Map)
        log.debug("Got multiplex query response: ${qry_response.body}");

      then:
        // Check the response looks OK generally
        assert qry_response.status == HttpStatus.OK
        assert qry_response.header(HttpHeaders.CONTENT_TYPE) == 'application/json;charset=UTF-8'
        assert qry_response.body().totalCount == 1
        // Check the shape of the returned JSON

        String multiplex_id = qry_response.body().resultList[0].id

      then:"Test that the elementsetname FULL setting works correctly on get"
        HttpRequest g1_request = HttpRequest.GET("/dabdata/multiplex/${multiplex_id}".toString())
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        g1_request.getParameters().add('setname','full');
        HttpResponse<Map> g1_response = client.toBlocking().exchange(g1_request,Map)
        log.debug("Got multiplex get response (g1): ${g1_response.body}");
        assert g1_response.body().id == multiplex_id
        assert g1_response.body().transmitters.size() == 2
        assert g1_response.body().bearers.size() == 12

      then:"Test that the elementsetname BRIEF setting works correctly on get"
        HttpRequest g2_request = HttpRequest.GET("/dabdata/multiplex/${multiplex_id}".toString())
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        g2_request.getParameters().add('setname','brief');
        HttpResponse<Map> g2_response = client.toBlocking().exchange(g2_request,Map)
        log.debug("Got multiplex get response (g2): ${g2_response.body}");
        assert g2_response.body().id == multiplex_id
        assert g2_response.body().containsKey('transmitters') == false
        assert g2_response.body().containsKey('bearers') == false
    }

    void testSecondIngest() {
      when:"We ingest some updates"
        HttpRequest load_request = HttpRequest.GET("/dabdata/loadDataFromUrl")
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        // load_request.getParameters().add('source','https://www.ofcom.org.uk/__data/assets/file/0018/91305/TxParamsDAB.csv');
        // lets reference the local file instead of pulling it every time
        load_request.getParameters().add('source','file:./src/test/resources/TxParamsDAB_sample_updated.csv');
        HttpResponse<Map> load_response = client.toBlocking().exchange(load_request,Map)
      then:"Test the updated columns"
        Map result = load_response.body()
        log.debug("Got response to GET message: ${load_response} / ${result}");
        assert result.rows_processed == 15
        assert result.multiplexes_created == 0
        assert result.multiplexes_updated == 15
        assert result.transmitters_created == 0
        assert result.transmitters_updated == 1 // We only report an update for changed data
        assert result.multiplex_transmitters_created == 0
        assert result.multiplex_transmitters_updated == 15
        assert result.broadcast_services_created == 0
        assert result.broadcast_services_updated == 314
        assert result.bearers_created == 0
        assert result.bearers_updated == 314
        assert result.bearer_errors == 0
        assert result.errors == 0
        assert result.status == 'OK'
    }

    void testAssertData() {
      when: "We assert some new OFCOM data"
        def broadcast_service_assertion = [
          type: 'broadcastService',
          options:[
            upsert:true
          ],
          match:[
            license:'CR000109BA'
          ],
          authorityData:[
            licensee: '10Radio CIC'
          ],
          supplementaryData:[
            name: '10 Radio'
          ]
        ]
 
        HttpRequest assert_request = HttpRequest.POST("/dabdata/admin/actions/assertData", broadcast_service_assertion)
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        HttpResponse<Map> assert_response = client.toBlocking().exchange(assert_request,Map)
      then:"Check that the assertion happened"
        Map result = assert_response.body()
        log.debug("Got response to assert message: ${assert_response} / ${result}");

    }

    void testAssertData2() {
      when: "We assert some new OFCOM data"
        def broadcast_service_assertion = [
          type: 'broadcastService',
          options:[
            upsert:true
          ],
          match:[
            fqdn:'dab:ce1.c1b5.c661.0'
          ],
          authorityData:[
            fqdn: 'dab:ce1.c1b5.c661.0',
            name: 'Heart Oxfordshire',
            authority: 'https://www.radiodns.uk/authorities/rdns.musicradio.com'
          ],
          supplementaryData:[
            logo128:'http://owdo.thisisglobal.com/2.0/id/138/logo/128x128.png'
          ]
        ]

        HttpRequest assert_request = HttpRequest.POST("/dabdata/admin/actions/assertData", broadcast_service_assertion)
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        HttpResponse<Map> assert_response = client.toBlocking().exchange(assert_request,Map)
      then:"Check that the assertion happened"
        Map result = assert_response.body()
        log.debug("Got response to assert message: ${assert_response} / ${result}");

    }

    void testAssertWithSameAs() {
      when: "We assert some new OFCOM data with a Same-As relationship"
        def broadcast_service_assertion = [
          type: 'broadcastService',
          options:[
            upsert:true
          ],
          match:[
            sid:'CA8A'
          ],
          authorityData:[
            sid: 'CA8A',
            name: 'Huntingdon Community Radio'
          ],
          sameAs:[
            OfcomLicense: ['CR000199BA', 'CR000199BA/4' ],
            WibbleValue: 'on2248sjkhhdfh'
          ]
        ]

        HttpRequest assert_request = HttpRequest.POST("/dabdata/admin/actions/assertData", broadcast_service_assertion)
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        HttpResponse<Map> assert_response = client.toBlocking().exchange(assert_request,Map)
      then:"Check that the assertion happened"
        Map result = assert_response.body()
        log.debug("Got response to assert message: ${assert_response} / ${result}");

    }

    void testMultiplexAssertData() {
      when: "We assert a new multiplex"
        def broadcast_service_assertion = [
          type: 'multiplex',
          options:[
            upsert:true
          ],
          match:[
            ead:'TESTMP001'
          ],
          authorityData:[
            ead: 'TESTMP001',
            licenseNumber: 'TEST1234'
          ],
          supplementaryData:[
            name: 'My Random Mplex',
          ]
        ]

        HttpRequest assert_request = HttpRequest.POST("/dabdata/admin/actions/assertData", broadcast_service_assertion)
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        HttpResponse<Map> assert_response = client.toBlocking().exchange(assert_request,Map)
      then:"Check that the assertion happened"
        Map result = assert_response.body()
        log.debug("Got response to assert message: ${assert_response} / ${result}");
    }

}
