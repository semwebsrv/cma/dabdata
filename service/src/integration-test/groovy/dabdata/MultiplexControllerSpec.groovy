package dabdata


import grails.testing.mixin.integration.Integration
import grails.testing.spock.OnceBefore
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification
import dabdata.MockCertificateManagerImpl
import org.springframework.beans.factory.annotation.Autowired
import groovy.util.logging.Slf4j
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.Claims;
import io.micronaut.http.client.DefaultHttpClient
import io.micronaut.http.client.HttpClientConfiguration
import io.micronaut.http.client.DefaultHttpClientConfiguration
import java.time.Duration;

@Slf4j
@Integration
class MultiplexControllerSpec extends Specification {

    @Shared
    @AutoCleanup
    HttpClient client

    // grails-app/conf/spring/resources.groovy arranges for certificateManager to be MockCertificateManagerImpl from the test src tree
    // this lets us mock the certificate rather than calling out to keycloak to get a real user token and makes our testing more
    // independent
    @Autowired
    MockCertificateManagerImpl certificateManager

    @Shared
    String session_jwt = null;

    @OnceBefore
    void init() {
        String baseUrl = "http://localhost:$serverPort"
        // this.client  = HttpClient.create(new URL(baseUrl))
        HttpClientConfiguration configuration = new DefaultHttpClientConfiguration()
        configuration.setConnectTimeout(Duration.ofSeconds(300))
        configuration.setReadTimeout(Duration.ofSeconds(300))
        this.client  = new DefaultHttpClient(new URL(baseUrl), configuration);

        def private_key = certificateManager.getPrivateKey()
        Date expiration = new Date(System.currentTimeMillis()+60000);
        List<String> roles = ['USER','ADMIN']

        this.session_jwt = Jwts.builder()
            .setIssuer("me")
            .setSubject("admin")
            .setAudience("you")
            .setId(UUID.randomUUID().toString()) //just an example id
            .setIssuedAt(new Date()) // for example, now
            .claim('roles', roles)
            .claim('realm-access', roles)
            .setExpiration(expiration) //a java.util.Date
            .setNotBefore(new Date()) //a java.util.Date 
            .signWith(private_key)
            .compact()

    }

    void listExistingMultiplexes() {
      when: "We call the index get endpoint"
        HttpRequest qry_request = HttpRequest.GET("/dabdata/multiplex")
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        qry_request.getParameters().add('q','name:Multiplex');
        HttpResponse<Map> qry_response = client.toBlocking().exchange(qry_request,Map)
      then: "We get back the current multiplexes"
        1==1
    }

    void testCreateMultiplex(String name, 
                             String ead, 
                             String area, 
                             String block, 
                             String frequency, 
                             String licenseNumber, 
                             String homepage,
                             int expected_query_response_count) {
      when:"We call the post endpoint"

        log.debug("Post Multiplex:${name} using JWT ${session_jwt}");

        // https://objectcomputing.com/files/6315/7297/3014/Slide_Deck_Micronaut_Declarative_HTTP_Client_Webinar.pdf
        Map new_multiplex = [ 
          'name' : name,
          'ead' : ead,
          'area' : area,
          'block' : block,
          'frequency' : frequency,
          'licenseNumber' : licenseNumber,
          'homepage' : homepage,
          'owner': getRootParty()
        ]
     
        HttpResponse<Map> response = client.toBlocking().exchange(HttpRequest.POST("/dabdata/multiplex", new_multiplex)
                                                                      .bearerAuth(session_jwt)
                                                                      .contentType(MediaType.APPLICATION_JSON_TYPE)
                                                                      .accept(MediaType.APPLICATION_JSON_TYPE),
                                                                  Map)

      then:"We use that JWT to access the multiplex endpoint"
        log.debug("Got response to POST message: ${response} / ${response.body}");

      then:"Check Number of multiplexes"
        HttpRequest qry_request = HttpRequest.GET("/dabdata/multiplex")
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        qry_request.getParameters().add('q','name:Multiplex');
        HttpResponse<Map> qry_response = client.toBlocking().exchange(qry_request,Map)
        log.debug("Got query response: ${qry_response.body}");
        log.debug("  Index result set size: ${qry_response.body().totalCount}, expected ${expected_query_response_count}")
        qry_response.body().resultList.each { m ->
          log.debug("${m.id} - ${m.name}");
        }
        qry_response.body().totalCount == expected_query_response_count
 
      where:
        name          | ead     | area                       | block | frequency | licenseNumber | homepage           | expected_query_response_count
        'Multiplex 1' | '1234'  | 'area-1'                   | 'b1'  | 'f1'      | '1234'        | 'http://1.2.3.5/a' | 1
        'Multiplex 2' | '1235'  | 'area-2'                   | 'b2'  | 'f3'      | '1236'        | 'http://1.2.3.5/b' | 2 
        'Multiplex 3' | '1236'  | 'area-3'                   | 'b3'  | 'f3'      | '1238'        | 'http://1.2.3.5/c' | 3 
        'Multiplex 4' | '1237'  | 'Multi Word Area Fluergle' | 'b3'  | 'f5'      | '1238'        | 'http://1.2.3.5/d' | 4
    }

    private String getRootParty() {
      String result = null;
      HttpRequest qry_request = HttpRequest.GET("/dabdata/party")
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
      qry_request.getParameters().add('q','shortcode:ROOT');
      HttpResponse<Map> qry_response = client.toBlocking().exchange(qry_request,Map)
      log.debug("Got query response: ${qry_response.body}");
      log.debug("  Index result set size: ${qry_response.body().totalCount}")
      if ( qry_response.body().totalCount == 1 ) {
        result = qry_response.body().resultList[0].id
      }
      else {
        throw new RuntimeException("Unable to locate ROOT party");
      }
      return result;

    }

    void testMultiplexSearch() {
      when:"We search based on name"
        HttpRequest qry_request = HttpRequest.GET("/dabdata/multiplex")
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        qry_request.getParameters().add('q','area:"multi word area fluergle"');
        HttpResponse<Map> qry_response = client.toBlocking().exchange(qry_request,Map)
        log.debug("Got query response: ${qry_response.body}");

      then:
        qry_response.status == HttpStatus.OK
        qry_response.header(HttpHeaders.CONTENT_TYPE) == 'application/json;charset=UTF-8'
        qry_response.body().totalCount == 1
    }

    void testMultiplexGetAnonymous() {
      when:"We get a specific multiplex"
        HttpRequest qry_request = HttpRequest.GET("/dabdata/public/multiplexes")
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        qry_request.getParameters().add('q','Multiplex');
        qry_request.getParameters().add('max','10');
        qry_request.getParameters().add('offset','0');
        HttpResponse<Map> qry_response = client.toBlocking().exchange(qry_request,Map)
        log.debug("Got query response: ${qry_response.body}");

      then:
        qry_response.status == HttpStatus.OK
        qry_response.header(HttpHeaders.CONTENT_TYPE) == 'application/json;charset=UTF-8'
        qry_response.body().totalCount == 4
    }

    void testMultiplexGetAuthenticated() {
      when:"We get a specific multiplex"
        HttpRequest get_request = HttpRequest.GET("/dabdata/public/multiplexes/1236")  // Get the aberdeen record
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        HttpResponse<Map> get_response = client.toBlocking().exchange(get_request,Map)
        log.debug("Got get response: ${get_response.body}");

      then:
        get_response.status == HttpStatus.OK
        get_response.header(HttpHeaders.CONTENT_TYPE) == 'application/json;charset=UTF-8'
    }

    void testMultiplexGetById() {
      when:"We search based on name"
        HttpRequest qry_request = HttpRequest.GET("/dabdata/multiplex")
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        qry_request.getParameters().add('q','area:"multi word area fluergle"');
        HttpResponse<Map> qry_response = client.toBlocking().exchange(qry_request,Map)
        log.debug("Got query response: ${qry_response.body}");

      then:
        qry_response.status == HttpStatus.OK
        qry_response.header(HttpHeaders.CONTENT_TYPE) == 'application/json;charset=UTF-8'
        qry_response.body().totalCount == 1
        def result_record = qry_response.body().resultList[0];

      then:"Issue a search with ID ${result_record?.id}"
        result_record != null;
        HttpRequest get_request_1 = HttpRequest.GET("/dabdata/multiplex/${result_record.id}".toString())
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        HttpResponse<Map> get_response_1 = client.toBlocking().exchange(get_request_1,Map)
        log.debug("Got query response: ${get_response_1.body}");

      then:
        get_response_1.status == HttpStatus.OK
        get_response_1.body().id == result_record.id

      then:"Fetch by EAD instead"
        HttpRequest get_request_2 = HttpRequest.GET("/dabdata/multiplex/${result_record.ead}".toString())
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        HttpResponse<Map> get_response_2 = client.toBlocking().exchange(get_request_2,Map)
        log.debug("Got query response: ${get_response_2.body}");

      then:
        get_response_2.status == HttpStatus.OK
        get_response_2.body().ead == result_record.ead

      when:"We try to update a record"
        result_record.frequency = 'f6'

      then:"check we can PUT by EAD too"
        HttpRequest put_request_1 = HttpRequest.PUT("/dabdata/multiplex/${result_record.ead}".toString(), result_record)
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        HttpResponse<Map> put_response_1 = client.toBlocking().exchange(put_request_1,Map)
        log.debug("Got query response: ${put_response_1.body}");
    }

}
