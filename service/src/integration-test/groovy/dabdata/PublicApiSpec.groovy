package dabdata


import grails.testing.mixin.integration.Integration
import grails.testing.spock.OnceBefore
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import dabdata.MockCertificateManagerImpl
import org.springframework.beans.factory.annotation.Autowired
import groovy.util.logging.Slf4j
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.Claims;
import java.time.Duration;
import io.micronaut.http.client.DefaultHttpClient 
import io.micronaut.http.client.HttpClientConfiguration
import io.micronaut.http.client.DefaultHttpClientConfiguration
import dabdata.Multiplex

@Slf4j
@Integration
@Stepwise
class PublicApiSpec extends Specification {

    @Shared
    @AutoCleanup
    HttpClient client

    @Shared
    Map<String,Map> stashed_data = [:]

    // grails-app/conf/spring/resources.groovy arranges for certificateManager to be MockCertificateManagerImpl from the test src tree
    // this lets us mock the certificate rather than calling out to keycloak to get a real user token and makes our testing more
    // independent
    @Autowired
    MockCertificateManagerImpl certificateManager

    @Shared
    String session_jwt = null;

    @OnceBefore
    void init() {
        String baseUrl = "http://localhost:$serverPort"
        // this.client  = HttpClient.create(new URL(baseUrl))
        HttpClientConfiguration configuration = new DefaultHttpClientConfiguration()
        configuration.setConnectTimeout(Duration.ofSeconds(300))
        configuration.setReadTimeout(Duration.ofSeconds(300))
        this.client  = new DefaultHttpClient(new URL(baseUrl), configuration);

        def private_key = certificateManager.getPrivateKey()
        Date expiration = new Date(System.currentTimeMillis()+60000);
        List<String> roles = ['USER','ADMIN','SOCIALAUTH','EDITOR']

        this.session_jwt = Jwts.builder()
            .setIssuer("me")
            .setSubject("admin")
            .setAudience("you")
            .setId(UUID.randomUUID().toString()) //just an example id
            .setIssuedAt(new Date()) // for example, now
            .claim('roles', roles)
            .claim('realm-access', roles)
            .claim('preferred_username', 'ian.ibbotson@semweb.co')
            .claim('email', 'ian.ibbotson@semweb.co')
            .setExpiration(expiration) //a java.util.Date
            .setNotBefore(new Date()) //a java.util.Date 
            .signWith(private_key)
            .compact()

    }


    void testSwaggerApiEndpoint() {
      when:"We call the swagger api endpoint"
        HttpRequest api_request = HttpRequest.GET("/dabdata/swagger/api");
        HttpResponse<Map> api_response = client.toBlocking().exchange(api_request,Map)

      then:"Check the docs"
        Map result = api_response.body()
        log.debug("Got response to API GET message: ${api_response} / ${result}");
    }

    void testRequestAccess(String resourceType, 
                           String resourceId, 
                           String resourceLabel, 
                           String authorityName, 
                           String authorityId, 
                           String requestedPerm, 
                           String message) {
      when:"We call the request access endpoint"
        HttpRequest access_request = HttpRequest.GET("/dabdata/selfService/requestAccess")
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        access_request.getParameters().add('resourceType',resourceType);
        access_request.getParameters().add('resourceId',resourceId);
        access_request.getParameters().add('resourceLabel',resourceLabel);
        access_request.getParameters().add('authorityName',authorityName);
        access_request.getParameters().add('authorityId',authorityId);
        access_request.getParameters().add('requestedPerm',requestedPerm);
        access_request.getParameters().add('message',message);
        HttpResponse<Map> access_response = client.toBlocking().exchange(access_request,Map)
        Map result = access_response?.body()
        stashed_data["AccessRequest-${resourceType}:${resourceId}".toString()] = [ id: result?.id ]
      then:
        access_response != null;
        log.debug("Got response to accessRequest: ${access_response} / ${result}");
      where:
        resourceType        | resourceId | resourceLabel | authorityName     | authorityId | requestedPerm | message
        'dabdata.Multiplex' | '123456'   | 'mp123456'    | 'BillDABCIC'      | null        | 'EDITOR'    | 'Hello'
        'dabdata.Multiplex' | '123457'   | 'mp123457'    | 'BillDABCIC'      | null        | 'EDITOR'    | 'Hello'
        'dabdata.Multiplex' | '123458'   | 'mp123458'    | 'OtherAuthority'  | null        | 'EDITOR'    | 'Hello'
    }

    void getStatusReport() {
      when:"We call the statusReport endpoint"
        HttpRequest qry_request = HttpRequest.GET("/dabdata/admin/statusReport")
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        HttpResponse<Map> qry_response = client.toBlocking().exchange(qry_request,Map)
        Map result = qry_response?.body()
        log.debug("Got status report: ${result}");
      then:"Show status report"
        result != null;
    }

    void testAccessRequestAdminApi() {
      when:"We call the list access request endpoint"
        HttpRequest qry_request = HttpRequest.GET("/dabdata/admin/accessRequests")
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        qry_request.getParameters().add('max','10');
        qry_request.getParameters().add('offset','0');
        qry_request.getParameters().add('q','%');
        HttpResponse<Map> qry_response = client.toBlocking().exchange(qry_request,Map)
        Map result = qry_response?.body()
        log.debug("Got query result: ${result}");

      then:"The three requests above are returned"
        result.totalCount == 3
    }

    void testMultiplexCreation() {
      when:"We create a test multiplex"
        Multiplex.withTransaction { status ->  
          Multiplex m = new Multiplex(name:'pubtestmp', owner: Party.findByShortcode('ROOT'), ead:'1238')
          m.save(flush:true, failOnError:true);
        }
      then:"there is a multiplex in the DB"
        Multiplex.withTransaction { status ->
          Multiplex.list().size() > 0;
        }
    }


    void testPublicMultiplexesEndpoint() {
      when:"We call the multiplexes endpoint"
        
        HttpRequest qry_request = HttpRequest.GET("/dabdata/public/multiplexes")
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        qry_request.getParameters().add('max','10');
        qry_request.getParameters().add('offset','0');
        qry_request.getParameters().add('q','%');
        HttpResponse<Map> qry_response = client.toBlocking().exchange(qry_request,Map)
        Map result = qry_response?.body()
        log.debug("Got query result: ${result}");

      then:"We get back a multiplex"
        result.totalCount > 0
    }

    void testPublicMultiplexEndpoint() {
      when:"We call the multiplex endpoint"
        HttpRequest qry_request = HttpRequest.GET("/dabdata/public/multiplexes/1238")
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        qry_request.getParameters().add('setname','full');
        HttpResponse<Map> qry_response = client.toBlocking().exchange(qry_request,Map)
        Map result = qry_response?.body()
        log.debug("Got query result: ${result}");

      then:"record returned"
        result != null;
    }

    void testPublicRefdataEndpoint() {
      when:"We call the refdata endpoint"
        HttpRequest qry_request = HttpRequest.GET("/dabdata/public/refdata/ResourceStatus")
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        HttpResponse<Map> qry_response = client.toBlocking().exchange(qry_request,Map)
        Map result = qry_response?.body()
        log.debug("Got refdata result: ${result}");

      then:"record returned"
        result != null;
        result?.values?.size() == 3
    }

    void testContactUs() {
      when:"We call the contact us endpoint"
        Map post_data = [
          sendername:'Fred',
          senderemail:'fred@a.b.c',
          content:'This is some text'
        ]

        HttpRequest qry_request = HttpRequest.POST("/dabdata/public/contactUs", post_data)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
        HttpResponse<Map> post_response = client.toBlocking().exchange(qry_request,Map)
        Map result = post_response?.body()
        log.debug("Got contactUs result: ${result}");

      then:"record returned"
        result != null;
    }
}
