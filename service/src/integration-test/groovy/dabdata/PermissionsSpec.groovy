package dabdata


import dabdata.Multiplex
import dabdata.MockCertificateManagerImpl
import grails.testing.mixin.integration.Integration
import grails.testing.spock.OnceBefore
import grails.gorm.transactions.Rollback
import java.time.Duration;
import spock.lang.Specification
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Stepwise
import groovy.util.logging.Slf4j
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.MediaType
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.Claims;
import io.micronaut.http.client.DefaultHttpClient
import io.micronaut.http.client.HttpClientConfiguration
import io.micronaut.http.client.DefaultHttpClientConfiguration
import org.springframework.beans.factory.annotation.Autowired

@Slf4j
@Integration
@Stepwise
@Rollback
class PermissionSpec extends Specification {

  @Shared
  @AutoCleanup
  HttpClient client

  // grails-app/conf/spring/resources.groovy arranges for certificateManager to be MockCertificateManagerImpl from the test src tree
  // this lets us mock the certificate rather than calling out to keycloak to get a real user token and makes our testing more
  // independent
  @Autowired
  MockCertificateManagerImpl certificateManager

  @Shared
  String session_jwt = null;

  @OnceBefore
  void init() {
    String baseUrl = "http://localhost:$serverPort"
    // this.client  = HttpClient.create(new URL(baseUrl))
    HttpClientConfiguration configuration = new DefaultHttpClientConfiguration()
    configuration.setConnectTimeout(Duration.ofSeconds(300))
    configuration.setReadTimeout(Duration.ofSeconds(300))
    this.client  = new DefaultHttpClient(new URL(baseUrl), configuration);

    def private_key = certificateManager.getPrivateKey()
    Date expiration = new Date(System.currentTimeMillis()+60000);
    List<String> roles = ['USER','ADMIN','ROLE_SOCIALAUTH']

    this.session_jwt = Jwts.builder()
      .setIssuer("me")
      .setSubject("admin")
      .setAudience("you")
      .setId(UUID.randomUUID().toString()) //just an example id
      .setIssuedAt(new Date()) // for example, now
      .claim('roles', roles)
      .claim('realm-access', roles)
      .claim('preferred_username', 'fred@some.isp')
      .setExpiration(expiration) //a java.util.Date
      .setNotBefore(new Date()) //a java.util.Date 
      .signWith(private_key)
      .compact()
  }


  String setupData() {

    String result = null;

    // We need these committed to the DB so the thread in the controller will pick them up
    Party.withNewTransaction { status ->
      def p = new Party(shortcode:'WIBBLE', name:'WIBBLE').save(flush:true, failOnError:true);
 
      def mp = new Multiplex(name:'TestMp', owner: p, ead:'TestMp').save(flush:true, failOnError:true);
      log.debug("Created multiplex: ${mp}");
      result = mp.id;

      def g = new Grant(
                grantResourceOwner:'/ROOT/WIBBLE',
                grantResourceType:'%',
                grantResourceId:'%',
                grantedPerm:'EDITOR',
                granteeType:'dabdata.Party',
                granteeId:'fred@some.isp').save(flush:true, failOnError:true);

      log.debug("Grants: ${Grant.list()}");
    }

    return result;
  }


  void 'test the permissions query'(multiplexEid, expectedResult) {

    given: 
      def mp_id = setupData();

    when:
      log.debug("Check the query works ${mp_id}");
      HttpRequest qry_request = HttpRequest.GET("/dabdata/multiplex/${mp_id}/userContext")
                                             .bearerAuth(session_jwt)
                                             .contentType(MediaType.APPLICATION_JSON_TYPE)
                                             .accept(MediaType.APPLICATION_JSON_TYPE);
      HttpResponse<Map> qry_response = client.toBlocking().exchange(qry_request,Map)
      def result = qry_response.body;
      log.debug("Got user context response: ${result}");
   
    then:
      result != null;

    where:
      multiplexEid|expectedResult
      'TestMp'|true
  }
}
