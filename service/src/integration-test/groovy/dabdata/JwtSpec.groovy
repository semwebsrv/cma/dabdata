package dabdata


import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.Claims;
import java.security.Key;
import java.security.PublicKey
import java.security.KeyFactory
import java.security.spec.X509EncodedKeySpec
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;



@Integration
@Rollback
class JwtSpec extends Specification {

    // See dev/lsp/lsp_service_assembly/attic/LifecycleSpec.groovy
    private static Map<String, Object> getRSAKeys() throws Exception {
      KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
      keyPairGenerator.initialize(2048);
      KeyPair keyPair = keyPairGenerator.generateKeyPair();
      PrivateKey privateKey = keyPair.getPrivate();
      PublicKey publicKey = keyPair.getPublic();
      Map<String, Object> keys = new HashMap<String, Object>();
      keys.put("private", privateKey);
      keys.put("public", publicKey);
      return keys;
    }

    def setup() {
    }

    def cleanup() {
    }

    void "Test Jwt Creation and Validation"() {
      when:"We Create a JWT"

        // String shared_secret_key = 'MUST_BE_256_BYTES__AABBCCDDEEFFGGHHIIKK__AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
        // Key key = Keys.hmacShaKeyFor(shared_secret_key.getBytes());

        def keys = getRSAKeys();
        def private_key = keys.'private'
        def public_key = keys.'public'


        Date expiration = new Date(System.currentTimeMillis()+60000);
        List<String> roles = ['__SYSTEM','USER','ROLE_SYSTEM']

        String jws = Jwts.builder()
            .setIssuer("me")
            .setSubject("admin")
            .setAudience("you")
            .setId(UUID.randomUUID().toString()) //just an example id
            .setIssuedAt(new Date()) // for example, now
            // .claim('tenant','__SYSTEM')
            .claim('roles', roles)
            .setExpiration(expiration) //a java.util.Date
            .setNotBefore(new Date()) //a java.util.Date 
            .signWith(private_key)
            .compact()
   
      then:"We can decode that JWT"
        // def validate(String jwt, String publickey_str) {
        // byte[] publicBytes = Base64.getDecoder().decode(publickey_str);
        // X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
        // KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        // PublicKey key = keyFactory.generatePublic(keySpec);

        // PublicKey publicKey = decodePublicKey(pemToDer(publickey));
        Claims claims = Jwts.parser()
             .setSigningKey(public_key)
             .parseClaimsJws(jws)
             .getBody();

      expect:"That the comains contain our original data"
            true == true
    }
}
