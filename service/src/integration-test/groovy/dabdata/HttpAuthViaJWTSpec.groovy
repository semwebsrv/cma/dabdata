package dabdata


import grails.testing.mixin.integration.Integration
import grails.testing.spock.OnceBefore
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpHeaders
import io.micronaut.http.HttpResponse
import io.micronaut.http.HttpStatus
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.exceptions.HttpClientResponseException;
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification
import dabdata.MockCertificateManagerImpl
import org.springframework.beans.factory.annotation.Autowired
import groovy.util.logging.Slf4j
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.Claims;

@Slf4j
@Integration
class HttpAuthViaJWTSpec extends Specification {

    @Shared
    @AutoCleanup
    HttpClient client

    // grails-app/conf/spring/resources.groovy arranges for certificateManager to be MockCertificateManagerImpl from the test src tree
    // this lets us mock the certificate rather than calling out to keycloak to get a real user token and makes our testing more
    // independent
    @Autowired
    MockCertificateManagerImpl certificateManager

    @OnceBefore
    void init() {
        String baseUrl = "http://localhost:$serverPort"
        this.client  = HttpClient.create(new URL(baseUrl))
    }

    void "Test the homepage"() {
      when:"The home page is requested"
        HttpResponse<Map> response = client.toBlocking().exchange(HttpRequest.GET("/dabdata/"), Map)

      then:"The response is correct"
        response.status == HttpStatus.OK
        response.header(HttpHeaders.CONTENT_TYPE) == 'application/json;charset=UTF-8'
        response.body().status == 'OK'
    }

    void "Test Certificate Mock"() {
      when:"We get the public and private certs"
        def public_cert = certificateManager.getPublicKey()
        def private_cert = certificateManager.getPrivateKey()

      then:"Check not null"
        public_cert != null;
        private_cert != null;
    }

    void testSecuredEndpointWithoutAuth() {
      when:"The /multiplex page is requested without an Authorization header"
        HttpStatus status = null;
        // HttpResponse response = client.toBlocking().exchange(HttpRequest.GET("/multiplex"))
        try {
          Object o = client.toBlocking().exchange(HttpRequest.GET("/dabdata/multiplex"))
        }
        catch ( io.micronaut.http.client.exceptions.HttpClientResponseException cre ) {
          status = cre.getStatus();
        }

      then:"The response is UNAUTHORIZED "
        // assertEquals(thrown.getStatus(), HttpStatus.UNAUTHORIZED);
        status == HttpStatus.UNAUTHORIZED 
        // response.header(HttpHeaders.CONTENT_TYPE) == 'application/json;charset=UTF-8'
        // response.body().count == 'OK'
    }

    void testSecuredEndpontWithLocallyGeneratedJWT() {
      when:"We generate a JWT"

        def private_key = certificateManager.getPrivateKey()
        Date expiration = new Date(System.currentTimeMillis()+60000);
        List<String> roles = ['USER','ADMIN']

        String jwt = Jwts.builder()
            .setIssuer("me")
            .setSubject("admin")
            .setAudience("you")
            .setId(UUID.randomUUID().toString()) //just an example id
            .setIssuedAt(new Date()) // for example, now
            .claim('roles', roles)
            .claim('realm-access', roles)
            .setExpiration(expiration) //a java.util.Date
            .setNotBefore(new Date()) //a java.util.Date 
            .signWith(private_key)
            .compact()

      then:"We use that JWT to access the multiplex endpoint"

        HttpResponse<Map> response = client.toBlocking().exchange(HttpRequest.GET("/dabdata/multiplex").bearerAuth(jwt), Map)
        log.debug("Got response ${response}");
    }
}
