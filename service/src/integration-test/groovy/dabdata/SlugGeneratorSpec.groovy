package dabdata


import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import dabdata.Multiplex
import groovy.util.logging.Slf4j
import spock.lang.Stepwise
import dabdata.Multiplex

@Slf4j
@Integration
@Rollback
class SlugGeneratorSpec extends Specification {

  @Autowired SlugGeneratorService slugGeneratorService

  void setupData() {
    // Set up some party entries with slugs that would clash with the one we are generating
    new Party(name:'P1', shortcode:'P1').save(flush:true, failOnError:true);
    new Party(name:'SMO', shortcode:'SMO').save(flush:true, failOnError:true);
  }


  void 'test slug generator service'(String domain,String field, String label, String expectedSlug) {
    given: 
      setupData()

    when:
      log.debug("Generate slug: ${domain},${field},${label}");
      def result = slugGeneratorService.getSlug(domain,field,label);
      log.debug("Generated slug ${result} for label ${label}");
      Party p = new Party(name: label, shortcode: result).save(flush:true, failOnError:true)
      log.debug("Made new party :${p} ${p.list()}");

    then:
      result == expectedSlug

    where:
      domain|field|label|expectedSlug
      'dabdata.Party'|'shortcode'|'P 1'|'P1_2'
      'dabdata.Party'|'shortcode'|'Party One Two Three Four'|'POTTF'
      'dabdata.Party'|'shortcode'|'Some Media Organisation'|'SMO_2'
  }
}
