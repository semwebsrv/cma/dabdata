package dabdata

import org.springframework.beans.factory.annotation.Autowired
import grails.core.GrailsApplication
import java.security.Key;
import java.security.PublicKey
import java.security.KeyFactory
import java.security.spec.X509EncodedKeySpec
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import static groovyx.net.http.HttpBuilder.configure
import groovyx.net.http.HttpBuilder
import groovyx.net.http.FromServer
import groovy.util.logging.Slf4j

@Slf4j
public class KeycloakCertificateManagerImpl implements CertificateManager {

  private Map tenant_public_key_map = [:]

  @Autowired
  GrailsApplication grailsApplication

  public PublicKey getPublicKey() {

    String realm = grailsApplication.config.keycloakRealm ?: 'RadioRegistry'

    PublicKey result = tenant_public_key_map[realm]
    if ( result == null ) {
      String base_64_encoded_public_key = getPublicKey(realm);
      if ( base_64_encoded_public_key != null ) {
        byte[] publicBytes = Base64.getDecoder().decode(base_64_encoded_public_key);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        result = keyFactory.generatePublic(keySpec);
        tenant_public_key_map[realm] = result;
      }
      else {
        throw new RuntimeException("Unable to resolve public key for ${realm} at ${grailsApplication.config.keycloakUrl}");
      }
    }

    return result
  }

  String getPublicKey(String realm) {

    String result = tenant_public_key_map.get(realm);

    if ( result == null ) {
      String keycloakUrl = grailsApplication.config.keycloakUrl ?: 'http://keycloak'
      HttpBuilder keycloak = configure { request.uri = keycloakUrl }
      log.info("** fetch keycloak ${realm} realm public cert from ${keycloakUrl}");

      def http_res = keycloak.get {
        request.uri.path = "/auth/realms/${realm}".toString()

        response.when(200) { FromServer fs, Object body ->
          // println("OK ${body}");
          if ( body?.public_key ) {
            result = body.public_key
            tenant_public_key_map.put(realm, body.public_key)
          }
          else {
            log.warn("Unable to find public key for ${realm}/${body}");
          }
        }
        response.failure { FromServer fs, Object body ->
          log.error("Problem ${body} ${fs} ${fs.getStatusCode()}");
        }
      }
    }

    return result;
  }

}
