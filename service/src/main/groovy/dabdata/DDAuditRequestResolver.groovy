package dabdata

import grails.plugins.orm.auditable.AuditLogContext
import groovy.transform.CompileStatic
import org.grails.web.servlet.mvc.GrailsWebRequest
import grails.plugins.orm.auditable.resolvers.AuditRequestResolver

class DDAuditRequestResolver implements AuditRequestResolver {

  @Override
  String getCurrentActor() {
    GrailsWebRequest request = GrailsWebRequest.lookup()
    def principal = request?.getUserPrincipal()?.getPrincipal()
    return ( principal?.username
      ?: principal?.sub 
      ?: AuditLogContext.context.defaultActor 
      ?: 'N/A' )
  }

  @Override
  String getCurrentURI() {
    GrailsWebRequest request = GrailsWebRequest.lookup()
    request?.request?.requestURI
  }
}
