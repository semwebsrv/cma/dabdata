package dabdata.oauth

import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter
import org.springframework.security.openid.OpenIDAuthenticationToken
import org.springframework.security.openid.OpenIDAttribute
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import java.security.PublicKey
import java.security.KeyFactory
import java.security.spec.X509EncodedKeySpec
import io.jsonwebtoken.Claims;
import groovyx.net.http.HttpBuilder
import groovyx.net.http.FromServer
import static groovyx.net.http.HttpBuilder.configure
import io.jsonwebtoken.Jwts;
import grails.core.GrailsApplication;
import grails.core.support.GrailsApplicationAware
import com.hazelcast.config.Config
import com.hazelcast.core.Hazelcast
import com.hazelcast.core.HazelcastInstance
import com.hazelcast.map.IMap
import java.util.concurrent.TimeUnit

import dabdata.CertificateManager

@Slf4j
class OAuthAuthenticationFilter extends AbstractAuthenticationProcessingFilter implements GrailsApplicationAware {
  
  private final JsonSlurper json = new JsonSlurper()

  private GrailsApplication grailsApplication
  // 30 minute expiry time on tenant public keys
  private HazelcastInstance instance = null;
  private IMap<String, Authentication> tokens = null;

  @Autowired
  CertificateManager certificateManager

  public OAuthAuthenticationFilter() {
    super("/**")
    this.authenticationSuccessHandler = new OAuthAuthenticationSuccessHandler()
    this.authenticationFailureHandler = new OAuthFailureHandler()
    this.allowSessionCreation = false
    log.debug("OAuthAuthenticationFilter::OAuthAuthenticationFilter complete");
  }

  public void setGrailsApplication(GrailsApplication grailsApplication) {
    this.grailsApplication = grailsApplication;
  }

  @javax.annotation.PostConstruct
  def init() {
    log.debug("Initialise OAuthAuthenticationFilter");
    def cfg = new Config('dabdata')
    instance = Hazelcast.getOrCreateHazelcastInstance(cfg)
    tokens = instance.getMap("authntok")
  }

  /* 
   * Only attempt auth if this request has the necessary headers. Otherwise leave it to filter down the chain.
   * @see org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter#requiresAuthentication(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  protected boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {

    boolean result = false;

    String authorization_header = request.getHeader('Authorization')

    // If we had an Authorization header and the path is marked as needed auth
    if ( authorization_header != null ) {
      result = super.requiresAuthentication(request,response);
    }

    // log.debug("OAuthAuthenticationFilter::requiresAuthentication returning authHeader:${authorization_header} result:${result}");
    
    return result;
  }
  
  /* 
   * @see org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter#attemptAuthentication(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
   */
  @Override
  public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
    
    Authentication result = null;
    String authorization_header = request.getHeader('Authorization')

    log.debug("OAuthAuthenticationFilter::attemptAuthentication");

    if ( ( authorization_header ) && 
         ( authorization_header.take(6).equalsIgnoreCase('bearer') ) ) {

      result = tokens.get(authorization_header) 

      if ( result == null ) {
        // log.debug("OAuthAuthenticationFilter::attemptAuthentication No cache hit on ${authorization_header?.take(20)}... ");
  
        // If we're passed a tenant ID, use that, otherwise validate against clusteradmin
        // String publickey = certificateManager.getPublicKey()
        // byte[] publicBytes = Base64.getDecoder().decode(publickey);
        // X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
        // KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        // PublicKey key = keyFactory.generatePublic(keySpec);
        PublicKey key = certificateManager.getPublicKey()
  
        try {
          Claims claims = Jwts.parser()
             .setSigningKey(key)
             .parseClaimsJws(authorization_header.substring(7)).getBody();
    
          java.util.Collection<GrantedAuthority> authorities = claims.realm_access?.roles.collect { new SimpleGrantedAuthority("ROLE_${it}") } +
                                                               claims.roles.collect { new SimpleGrantedAuthority("ROLE_${it}") }
          log.debug("calculated authorities: ${authorities}");
          // log.info("Claims: ${claims}");
  
          // Turns out that claims cannot be serialized which can cause problems when caching the token. We convert it to a plain old map first
          Map<String, Object> plain_old_map_of_claims = new HashMap<String, Object>();
          claims.each { cl_key, cl_value ->
            plain_old_map_of_claims.put(cl_key,cl_value);
          }
  
          plain_old_map_of_claims.username = plain_old_map_of_claims.preferred_username;

          String identityUrl = null;
  
          OpenIDAuthenticationToken authRequest = new OpenIDAuthenticationToken(
            plain_old_map_of_claims,
            authorities,
            identityUrl,
            [] // claims.collect { new OpenIDAttribute(it.key, it.value ) }
          )
    
          result = getAuthenticationManager().authenticate(authRequest)
          if ( ( result != null ) && ( result.isAuthenticated() ) ) {
            // For now - cache tokens for 10 mins
            tokens.put(authorization_header, result, 600L, TimeUnit.SECONDS)
          }
          else {
            log.warn("Authentication failed. ${result}");
          }
        }
        catch ( io.jsonwebtoken.MalformedJwtException malformed_jwt_exception ) {
          log.error("Malformed JWT");
          throw new org.springframework.security.authentication.BadCredentialsException('Malformed JWT')
        }
        catch ( io.jsonwebtoken.ExpiredJwtException expired_jwt_exception ) {
          log.error("Expired JWT");
          throw new org.springframework.security.authentication.BadCredentialsException('Expired JWT')
        }
      }
      else {
        log.debug("Cache hit ${authorization_header} - tokens.size() == ${tokens.size()}");
      }
    }
    else {
      log.debug("No bearer token. Auth header value was: \"${authorization_header}\"... skip");
    }

    return result;
  }
  
  @Override
  protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult)
  throws IOException, ServletException {
    super.successfulAuthentication(request, response, chain, authResult)

    // As we are authenticating using header information, we need to continue with the filter chain to ensure the request is
    // properly actioned.
    try {
      chain.doFilter(request, response)
    }
    catch ( Exception e ) {
      log.debug("Exception in filter chain",e)
      throw e;
    }
  }
}
