package dabdata.oauth

import javax.annotation.PostConstruct
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.beans.factory.support.BeanDefinitionRegistry
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.core.AuthenticationException

import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.access.AccessDeniedHandler
import org.springframework.security.web.authentication.AuthenticationFailureHandler

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.security.openid.OpenIDAuthenticationToken
import java.lang.Thread


// See https://www.baeldung.com/spring-security-custom-authentication-failure-handler

@CompileStatic
@Slf4j
class OAuthFailureHandler implements AuthenticationFailureHandler {

  @Override
  public void onAuthenticationFailure( HttpServletRequest request, HttpServletResponse response, AuthenticationException exception) throws IOException, ServletException {
    log.debug("OAuthFailureHandler::onAuthenticationFailure");
    response.sendError HttpServletResponse.SC_INTERNAL_SERVER_ERROR, exception.message

  }

}

