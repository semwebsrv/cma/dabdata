package dabdata.oauth

import javax.annotation.PostConstruct
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.config.BeanDefinition
import org.springframework.beans.factory.config.ConfigurableBeanFactory
import org.springframework.beans.factory.support.BeanDefinitionRegistry
import org.springframework.security.access.AccessDeniedException
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.access.AccessDeniedHandler

import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.security.openid.OpenIDAuthenticationToken
import java.lang.Thread

@CompileStatic
@Slf4j
class OAuthAwareAccessDeniedHandler implements AccessDeniedHandler {

  @Override
  void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException e) throws IOException, ServletException {

    log.debug("OAuthAwareAccessDeniedHandler::handle");

    if (authentication && OpenIDAuthenticationToken.class.isAssignableFrom(authentication.class)) {
      log.debug("Sending 403 for Holo request without attempting forward. message will be ${e.message}");
      response.sendError HttpServletResponse.SC_FORBIDDEN, e.message
      return
    }
    else {
      log.warn("authenitcation not assignable from OpenIDAuthenticationToken....");
    }

    // log.debug("Fall back to previous access denied handler. authenitcaiton object class is ${authentication?.class?.name}");
    // Otherwise we just delegate to the previous handler.
    // previousAccessDeniedHandler?.handle(request, response, e) // ELSE NOOP.
  }

  protected Authentication getAuthentication() {
    SecurityContextHolder.context?.authentication
  }
}

