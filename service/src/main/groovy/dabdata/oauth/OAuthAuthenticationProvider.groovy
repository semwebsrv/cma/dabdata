package dabdata.oauth

import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.core.Authentication
import org.springframework.security.core.AuthenticationException
import org.springframework.security.web.util.matcher.RequestMatcher
import org.springframework.util.Assert
import org.springframework.security.openid.OpenIDAuthenticationToken

import groovy.util.logging.Slf4j

@Slf4j
class OAuthAuthenticationProvider implements AuthenticationProvider {

  @Override
  public boolean supports(Class<?> authentication) {
    log.debug("Checking ${authentication} is assignable for OpenIDAuthenticationToken");
    OpenIDAuthenticationToken.class.isAssignableFrom(authentication)
  }

  @Override
  public Authentication authenticate(Authentication authentication) throws AuthenticationException {

    log.debug("authenticate ${authentication}");

    // Return null if we don't support this token.
    if (!supports(authentication.class)) return null

    OpenIDAuthenticationToken authToken = (OpenIDAuthenticationToken) authentication

    // Ideally we could with some form of check here.
    authToken.setAuthenticated(true)

    authToken
  }
}

