package dabdata


import grails.rest.*
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured
import dabdata.Multiplex;
import grails.orm.PagedResultList
import groovy.util.logging.Slf4j
import grails.gorm.transactions.Transactional

import grails.web.http.HttpHeaders
import org.springframework.http.HttpStatus
import static org.springframework.http.HttpStatus.*

import org.grails.web.servlet.mvc.GrailsWebRequest


@Slf4j
class DDRestfulController<T> extends RestfulController<T> {
  static responseFormats = ['json', 'xml']

  DDRestfulController(Class<T> domainClass) {
    this(domainClass, false)
  }

  DDRestfulController(Class<T> domainClass, boolean readOnly) {
    super(domainClass, readOnly)
  }

  @Override
  @Secured('ROLE_USER')
  def index() { 
    Map result = null;
    log.debug("DDRestfulController::index() starting q=${params.q}");

    if ( ( params.q != null ) && ( params.q.length() > 0 ) ) {
      grails.orm.PagedResultList prl = resource.findAllByLuceneQueryString(params.q, params)
      log.debug("DDRestfulController::index() create result");
      if ( prl != null ) {
        result = [ 
          totalCount: prl.totalCount,
          resultList: prl.resultList
        ]
      }
      else {
        result = [ totalCount:0, message:'internal error PRL is null' ]
      }
    }
    else {
      result = [
        totalCount:0,
        message:'no-query-supplied'
      ]
    }

    log.debug("DDRestfulController::index() returning");
    // render result as JSON
    respond result
  }

  @Override
  @Secured('ROLE_USER')
  def show() {
    super.show()
  }

  @Override
  @Secured(['ROLE_EDITOR', 'ROLE_ADMIN'])
  def create() {
    super.create()
  }

  @Override
  @Secured(['ROLE_EDITOR', 'ROLE_ADMIN'])
  def edit() {
    super.edit()
  }


  @Override
  @Secured(['ROLE_EDITOR', 'ROLE_ADMIN'])
  def save() {
    log.debug("Calling save on ${resource}");
    super.save()
  }
  
  @Transactional
  @Override
  @Secured(['ROLE_EDITOR', 'ROLE_ADMIN'])
  def update() {

    // Brought in defintion from grails.rest.RestfulController at
    // https://jar-download.com/artifacts/org.grails/grails-plugin-rest/2.5.0/source-code/grails/rest/RestfulController.groovy


      if(handleReadOnly()) {
        return
      }

      T instance = queryForResource(params.id)
      if (instance == null) {
        transactionStatus.setRollbackOnly()
        notFound()
        return
      }

      log.info("params.version: ${params.version} resource.version: ${instance.version}");
      log.info("principal/subject: ${getPrincipal()} ${getPrincipal()?.sub}");

      // GrailsWebRequest gr = GrailsWebRequest.lookup()
      // log.info("gwr_user: gr.userPrincipal : ${gr?.userPrincipal} ${gr?.userPrincipal?.class?.name}");   
      // log.info("gwr_user: gr.getUserPrincipal().getPrincipal() : ${gr?.getUserPrincipal()?.getPrincipal()?.sub}");

      
      



      instance.properties = getObjectToBind()

      if (instance.hasErrors()) {
        transactionStatus.setRollbackOnly()
        respond instance.errors, view:'edit' // STATUS CODE 422
        return
      }

      if ( instance.metaClass.hasProperty(instance, 'lastUpdatedBy' ) ) {
        instance.lastUpdatedBy = getPrincipal()?.sub
      }

      instance.save flush:true

      request.withFormat {
        form multipartForm {
          flash.message = message(code: 'default.updated.message', args: [message(code: "${resourceClassName}.label".toString(), default: resourceClassName), instance.id])
          redirect instance
        }
        '*'{
          response.addHeader(HttpHeaders.LOCATION,
                            g.createLink(
                                  resource: this.controllerName, action: 'show',id: instance.id, absolute: true,
                                  namespace: hasProperty('namespace') ? this.namespace : null ))
           respond instance, [status: OK]
        }
      }

  }

  @Override
  @Secured('ROLE_ADMIN')
  def delete() {
    super.delete()
  }

}
