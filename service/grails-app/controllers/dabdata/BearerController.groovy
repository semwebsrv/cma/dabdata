package dabdata


import grails.rest.*
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured
import dabdata.Bearer;

class BearerController extends DDRestfulController<Bearer> {

  static responseFormats = ['json', 'xml']

  BearerController() {
    super(Bearer);
  }
	
}
