package dabdata

import grails.core.GrailsApplication
import grails.plugins.*
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured


class ApplicationController implements PluginManagerAware {

  GrailsApplication grailsApplication
  GrailsPluginManager pluginManager
  def ETLHelperService

  def index() {
    log.debug("ApplicationController::index");
    def result = [ 
      status: 'OK',
      appVersion: grailsApplication.metadata.getApplicationVersion(),
      grailsVersion: grailsApplication.metadata.getGrailsVersion(),
      buildTime: grailsApplication.metadata['build.time']
    ]
    render result as JSON
  }

  @Secured('ROLE_ADMIN')
  def loadDataFromUrl(String source) {

    Map result = null;

    log.debug("ApplicationController::loadDataFromUrl(${source})");
    if ( ETLHelperService ) {
      if ( ETLHelperService.validateURL(source) ) {
        result = ETLHelperService.ingestURL(source)
        result.status='OK'
      }
    }
    else {
      log.error("csvIngestService not found, unable to process");
    }

    if ( result==null )
      result = [ status: 'ERROR' ]

    render result as JSON
  }

  @Secured('ROLE_ADMIN')
  def statusReport() {
    def result = [:]

    result.pendingAccessRequests=AccessRequest.executeQuery('select count(ar) from AccessRequest as ar').get(0);

    render result as JSON
  }
}
