package dabdata

import grails.core.GrailsApplication
import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured

class SelfServiceApiController {

  static responseFormats = ['json', 'xml']
  def mailService

  @Secured(['ROLE_SOCIALAUTH', 'ROLE_EDITOR', 'ROLE_ADMIN'])
  def requestAccess(String resourceType,
                    String resourceId,
                    String resourceLabel,
                    String authorityName,
                    String authorityId,
                    String requestedPerm,
                    String message) {

    // User fred@some.domain wishes to have EDITOR             access to   Transmitter:1234    and is a member of "Org-Owning-Trans-4"  Which has ID 4
    //      --from creds--                  --requested Perm--             -resType-   -resId-                   -authorityName-                     -authorityId-     
    // OR
    // User jim@some.domain wishes to have EDITOR access to Transmitter:2345. Transmitter 2345 does not currently have an owner, and Jim is requesting
    //         creation of a new authority - "Org-Owning-Transmitter-2345" to manage access to that record

    log.debug("requestAccess(${resourceType},${resourceId},${resourceLabel},${authorityName},${authorityId},${requestedPerm},${message})");
    log.info("Principal: ${getPrincipal()} ${getPrincipal()?.username}");
    log.info("referer: ${request.getHeader('referer')}");

    def result = [:]
    if ( ( getPrincipal()?.username != null ) &&
         ( ( authorityId != null ) || ( authorityName != null ) ) &&
         ( resourceType != null ) &&
         ( resourceId != null ) &&
         ( resourceLabel != null ) &&
         ( requestedPerm != null ) ) {
      AccessRequest.withTransaction { status ->
        AccessRequest ar = new AccessRequest(
                                 userName:getPrincipal()?.username,
                                 authorityName:authorityName,
                                 authorityId:authorityId,
                                 requestedPerm: requestedPerm,
                                 resourceType: resourceType,
                                 resourceId: resourceId,
                                 resourceLabel: resourceLabel,
                                 notes: message,
                                 dateOfRequest: new Date(),
                                 actionedBy: null,
                                 dateActioned: null,
                                 status: 'PENDING',
                                 reason: null).save(flush:true, failOnError:true)

        log.debug("Created access request: ${ar}");
        result.status = 'Created'
        result.id = ar.id
        result.message = "Created new access request - ${ar.id}"
      }

      sendEmailNotification(getPrincipal()?.username, requestedPerm, authorityName, message)
    }
    else {
      result.status = 'error'
      result.message = 'The system needs an authority name or Id to create an access request'
    }
    render result as JSON
  }

  private void sendEmailNotification(String username, String requestedPerm, String authorityName, String message) {
    List<String> contacts = grailsApplication.config.getProperty('radioregister.contactAddr')?.split(",").collect{it.trim()};

      // See https://grails3-plugins.github.io/mail/guide/2.%20Configuration.html#2.1%20SMTP%20Server%20Configuration
      // Note well grails.mail.disabled = true set for dev and test - if you wish to test email sending adjust in application.yml
      mailService.sendMail {
        to contacts
        from grailsApplication.config.getProperty('radioregister.senderAddr')
        // cc "marge@gmail.com", "ed@gmail.com"
        // bcc "joe@gmail.com"
        subject "RR : User ${username} is requesting ${requestedPerm} access to ${authorityName}"
        text """
The request was sent to ${request.getHeader('referer')} - visit ${request.getHeader('referer')}/admin/pendingAccessRequests to resolve.
-----
${message}
"""
      }

  }

  // Party      Res Type   Res ID    GranteeType              GranteeId          RequiredPerm
  @Secured(['ROLE_EDITOR', 'ROLE_ADMIN'])
  def listUserPermissionsFor(String resourceType,
                             String resourceId) {
    def result = [ 'grants': ['Editor'] ]
    render result as JSON
  }

  @Secured(['ROLE_EDITOR', 'ROLE_ADMIN'])
  def userIsGranted(String permission,
                    String resourceType,
                    String resourceId) {
    def result = [ 'result': true ]
    render result as JSON
  }
}
