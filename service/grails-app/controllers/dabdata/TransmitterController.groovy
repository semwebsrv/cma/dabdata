package dabdata


import grails.rest.*
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured
import dabdata.Transmitter;

class TransmitterController extends DDRestfulController<Transmitter> {

  static responseFormats = ['json', 'xml']

  TransmitterController() {
    super(Transmitter);
  }
	
}
