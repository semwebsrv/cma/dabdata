package dabdata

import grails.core.GrailsApplication
import dabdata.Transmitter;
import dabdata.Multiplex;
import dabdata.BroadcastService;
import dabdata.Party;

// import java.text.SimpleDateFormat;
// SimpleDateFormat hrsdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss 'UTC'")


class PublicApiController {

  static responseFormats = ['json', 'xml']

  def mailService



  def transmitters() {
    Map result = null;
    log.debug("PublicApiController::transmitters() starting q=${params.q}");

    if ( ( params.q != null ) && ( params.q.length() > 0 ) ) {
      grails.orm.PagedResultList prl = Transmitter.findAllByLuceneQueryString(params.q, params)
      if ( prl != null ) {
        result = [
          totalCount: prl.totalCount,
          resultList: prl.resultList
        ]
      }
      else {
        result = [ totalCount:0, message:'internal error PRL is null' ]
      }
    }
    else {
      result = [
        totalCount:0,
        message:'no-query-supplied'
      ]
    }

    log.debug("PublicApiController::transmitters() returning");
    // render result as JSON
    respond result
  }

  def transmitter() {
    def result = Transmitter.get(params.id)

    if ( result == null ) {
      result = Transmitter.findBySlug(params.id)
    }

    respond result;
  }

  def multiplexes() {
    Map result = null;
    log.debug("PublicApiController::multiplexes() starting q=${params.q}");

    if ( ( params.q != null ) && ( params.q.length() > 0 ) ) {
      grails.orm.PagedResultList prl = Multiplex.findAllByLuceneQueryString(params.q, params)
      if ( prl != null ) {
        result = [
          totalCount: prl.totalCount,
          resultList: prl.resultList
        ]
      }
      else {
        result = [ totalCount:0, message:'internal error PRL is null' ]
      }
    }
    else {
      result = [
        totalCount:0,
        message:'no-query-supplied'
      ]
    }

    log.debug("PublicApiController::multiplexes() returning");
    // render result as JSON
    respond result
  }

  def multiplex() {
    def mp = Multiplex.get(params.id)

    if ( mp == null ) {
      mp = Multiplex.findByEad(params.id)
    }

    // log.info("Principal: ${getPrincipal()} classname:${getPrincipal()?.class?.name}");
    respond(mp)
  }

  def services() {
    Map result = null;
    log.debug("PublicApiController::services() starting q=${params.q}");

    if ( ( params.q != null ) && ( params.q.length() > 0 ) ) {
      grails.orm.PagedResultList prl = BroadcastService.findAllByLuceneQueryString(params.q, params)
      if ( prl != null ) {
        result = [
          totalCount: prl.totalCount,
          resultList: prl.resultList
        ]
      }
      else {
        result = [ totalCount:0, message:'internal error PRL is null' ]
      }
    }
    else {
      result = [
        totalCount:0,
        message:'no-query-supplied'
      ]
    }

    log.debug("PublicApiController::services() returning");
    // render result as JSON
    respond result
  }

  def service() {
    // def result = BroadcastService.findBySid(params.id)
    def result = BroadcastService.get(params.id)
    if ( result == null ) {
      result = BroadcastService.findBySlug(params.id)
    }
    if ( result == null ) {
      result = BroadcastService.findBySid(params.id)
    }
    
    respond result;
  }

  def parties() {
    Map result = null;
    log.debug("PublicApiController::parties() starting q=${params.q}");

    if ( ( params.q != null ) && ( params.q.length() > 0 ) ) {
      grails.orm.PagedResultList prl = Party.findAllByLuceneQueryString(params.q, params)
      if ( prl != null ) {
        result = [
          totalCount: prl.totalCount,
          resultList: prl.resultList
        ]
      }
      else {
        result = [ totalCount:0, message:'internal error PRL is null' ]
      }
    }
    else {
      result = [
        totalCount:0,
        message:'no-query-supplied'
      ]
    }

    log.debug("PublicApiController::parties() returning");
    // render result as JSON
    respond result
  }

  def party() {
    def result = Party.findByShortcode(params.id)
    respond result;
  }

  def refdata(String category) {
    log.debug("PublicApiController::refdata(${category})");
    def values = RefdataValue.executeQuery('select rv from RefdataValue as rv where rv.owner.desc = :o order by rv.order, rv.value',[o:category])
    Map result = [
      'category': category,
      'values': values.collect { [ id:it.id, value:it.value, label:it.label ] }
    ]
    respond result;
  }

  def contactUs() {
    log.debug("PublicApiController::contactUs(${params})");

    List<String> contacts = grailsApplication.config.getProperty('radioregister.contactAddr')?.split(",").collect{it.trim()};
    log.debug("Sending to ${contacts}");

    if ( ( request.JSON?.sendername != null ) &&
         ( request.JSON?.senderemail != null ) &&
         ( request.JSON?.content != null ) &&
         ( contacts != null ) &&
         ( contacts.size() > 0 ) ) {

      // See https://grails3-plugins.github.io/mail/guide/2.%20Configuration.html#2.1%20SMTP%20Server%20Configuration
      // Note well grails.mail.disabled = true set for dev and test - if you wish to test email sending adjust in application.yml
      mailService.sendMail {
        to contacts
        from grailsApplication.config.getProperty('radioregister.senderAddr')
        // cc "marge@gmail.com", "ed@gmail.com"
        // bcc "joe@gmail.com"
        subject "User ${request.JSON.sendername} ${request.JSON.senderemail} Completed the contactus form on ${new Date()}"
        text request.JSON.content
      }
    }
    else {
      log.warn("Missing information for contact form contacts=${contacts}");
      log.warn("sender=${request.JSON.sendername} email=${request.JSON.senderemail} content=${request.JSON.content} contacts.size=${contacts?.size()}");
    }

    Map result = [
      'status':'OK'
    ]
    respond result;
  }
}
