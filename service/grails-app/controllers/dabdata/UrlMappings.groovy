package dabdata

class UrlMappings {

    static mappings = {
        // delete "/$controller/$id(.$format)?"(action:"delete")
        // get "/$controller(.$format)?"(action:"index")
        // get "/$controller/$id(.$format)?"(action:"show")
        // post "/$controller(.$format)?"(action:"save")
        // put "/$controller/$id(.$format)?"(action:"update")
        // patch "/$controller/$id(.$format)?"(action:"patch")

        "/"(controller: 'application', action:'index')
        "/swagger/api" (controller: "swagger", action: "api")
        "/loadDataFromUrl"(controller: 'application', action:'loadDataFromUrl')
        "/bearer"(resources:'bearer')
        "/broadcastService"(resources:'broadcastService')
        "/multiplex"(resources:'multiplex') {
          "/userContext" (action: 'userContext', method: 'GET')
        }
        "/party"(resources:'party')
        "/transmitter"(resources:'transmitter')

        "/admin/accessRequests"(resources:'accessRequest')
        "/admin/statusReport"(controller: 'application', action:'statusReport')
        "/admin/actions/$action"(controller: 'admin')

        "/public/parties"(controller:'publicApi', action:'parties')
        "/public/parties/$id"(controller:'publicApi', action:'party')

        "/public/transmitters"(controller:'publicApi', action:'transmitters')
        "/public/transmitters/$id"(controller:'publicApi', action:'transmitter')

        "/public/multiplexes"(controller:'publicApi', action:'multiplexes')
        "/public/multiplexes/$id"(controller:'publicApi', action:'multiplex')

        "/public/services"(controller:'publicApi', action:'services')
        "/public/services/$id"(controller:'publicApi', action:'service')

        "/public/refdata/$category"(controller:'publicApi', action:'refdata')

        "/public/contactUs"(controller:'publicApi', action:'contactUs')

        "/selfService/$action"(controller:'selfServiceApi')

        "/user/$action"(controller:'user')

        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
