package dabdata


import grails.rest.*
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured
import dabdata.BroadcastService;

class BroadcastServiceController extends DDRestfulController<BroadcastService> {

  static responseFormats = ['json', 'xml']

  BroadcastServiceController() {
    super(BroadcastService);
  }
	
  @Override
  protected BroadcastService queryForResource(Serializable id) {
    BroadcastService result = null;
    result = BroadcastService.get(id);
    if ( result == null ) {
      result = BroadcastService.findBySlug(id)
    }
    if ( result == null ) {
      result = BroadcastService.findBySid(id)
    }
    return result;
  }

}
