package dabdata

import grails.core.GrailsApplication
import grails.plugins.*
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured


class UserController {

  GrailsApplication grailsApplication

  // List all the broadcast services where the user is granted EDITOR permission (Directly or via a wildcard)
  private static final String BROADCAST_SERVICE_QUERY = '''
select bs 
from BroadcastService as bs
where EXISTS (
       select g from Grant as g 
         where (
               bs.owner.fqshortcode like g.grantResourceOwner
           and 'dabdata.BroadcastService' like g.grantResourceType
           and bs.id like g.grantResourceId
         ) 
         and ( 
              ( g.granteeType = 'dabdata.Party' and g.granteeId = :userid )
         )
         and :reqperm like g.grantedPerm
     )
order by bs.name

'''

private static final String MULTIPLEX_QUERY = '''
select m 
from Multiplex as m
where EXISTS (
       select g from Grant as g 
         where (
               m.owner.fqshortcode like g.grantResourceOwner
           and 'dabdata.Multiplex' like g.grantResourceType
           and m.id like g.grantResourceId
         ) 
         and ( 
              ( g.granteeType = 'dabdata.Party' and g.granteeId = :userid )
         )
         and :reqperm like g.grantedPerm
     )
order by m.name
'''

private static final String TRANSMITTER_QUERY = '''
select t
from Transmitter as t
where EXISTS (
       select g from Grant as g 
         where (
               t.owner.fqshortcode like g.grantResourceOwner
           and 'dabdata.Transmitter' like g.grantResourceType
           and t.id like g.grantResourceId
         ) 
         and ( 
              ( g.granteeType = 'dabdata.Party' and g.granteeId = :userid )
         )
         and :reqperm like g.grantedPerm
     )
order by t.name
'''


  @Secured('ROLE_USER')
  def index() {
    log.debug("UserController::index");
    Map result = [ 
      'status':'OK'
    ]
    render result as JSON
  }

  @Secured('ROLE_USER')
  def getHomePageData() {
    log.debug("UserController::getHomePageData (${getPrincipal()?.username})");

    def grant_critera = Grant.createCriteria();
    def user_grants = grant_critera.list {
      and {
        eq('granteeId', getPrincipal()?.username)
        eq('granteeType', 'dabdata.Party')
      }
      order('grantResourceOwner','asc');
    }

    def user_services = BroadcastService.executeQuery(BROADCAST_SERVICE_QUERY,[userid:getPrincipal()?.username, reqperm:'EDITOR']);
    log.debug("Located services: ${user_services}");

    def user_multiplexes = Multiplex.executeQuery(MULTIPLEX_QUERY,[userid:getPrincipal()?.username, reqperm:'EDITOR']);
    log.debug("Located multiplexes: ${user_multiplexes}");

    def user_transmitters = Transmitter.executeQuery(TRANSMITTER_QUERY,[userid:getPrincipal()?.username, reqperm:'EDITOR']);
    log.debug("Located transmitters: ${user_transmitters}");

    // Retrieve user affiliations
    Map result = [ 
      'status':'OK',
      'userGrants': user_grants.collect { [ 
                                            grantResourceOwner: it.grantResourceOwner, 
                                            grantResourceType: it.grantResourceType, 
                                            grantResourceId: it.grantResourceId,
                                            grantedPerm: it.grantedPerm,
                                            granteeType: it.granteeType,
                                            granteeId: it.granteeId
                                          ] },
      'userServices':user_services.collect  { [ id: it.id, name: it.name, slug: it.slug, owner: it.owner?.name, ownerfqshortcode:it.owner?.fqshortcode ] },
      'userMultiplexes':user_multiplexes.collect { [ id:it.id, name:it.name, ead:it.ead, owner: it.owner?.name, ownerfqshortcode:it.owner?.fqshortcode ] },
      'userTransmitters': user_transmitters.collect { [ id:it.id, name:it.name, slug: it.slug, owner: it.owner?.name, ownerfqshortcode:it.owner?.fqshortcode ] } 
    ]
    render result as JSON
  }

}
