package dabdata


import grails.rest.*
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured
import dabdata.Multiplex;
import grails.plugin.springsecurity.annotation.Secured



class MultiplexController extends DDRestfulController<Multiplex> {

  // Count all the grants where our user has a permission which allows editing of the multiplex in question
  def GRANT_QUERY='''
select count(g.id)
from dabdata.Grant as g 
where g.granteeId=:principal
  and g.granteeType='dabdata.Party'
  and g.grantedPerm='EDITOR'
  and :multiplexOwner like g.grantResourceOwner
  and 'dabdata.Multiplex' like g.grantResourceType
  and :multipexId like g.grantResourceId
'''

  static responseFormats = ['json', 'xml']

  MultiplexController() {
    super(Multiplex);
  }

  @Secured(['ROLE_SOCIALAUTH', 'ROLE_USER', 'ROLE_ADMIN'])
  def userContext() {
    log.info("MultiplexController::userContext() principal=${getPrincipal()} multiplexId:${params.multiplexId} ${params}");
    def result = [:]
    String username = getPrincipal()?.username;

    Multiplex mp = Multiplex.get(params.multiplexId)

    if ( mp ) {
      log.debug("Located multiplex, owner party is : ${mp.owner.fqshortcode}");
      result.status='OK'
      // Does the user have EDITOR rights to access this multiplex?
      def c = Grant.executeQuery(GRANT_QUERY,[principal:username, multiplexOwner:mp.owner.fqshortcode, multipexId: params.multiplexId]).get(0);
      result.userIsEditor = c > 0 ? true : false;
    }
    else {
      result.status='ERROR'
      result.message="unable to locate multiplex with id ${params.multiplexId}"
    }

    render result as JSON;
  }

  @Override
  protected Multiplex queryForResource(Serializable id) {
    Multiplex result = null;
    result = Multiplex.get(id);
    if ( result == null ) {
      result = Multiplex.findByEad(id)
    }
    return result;
  }
}
