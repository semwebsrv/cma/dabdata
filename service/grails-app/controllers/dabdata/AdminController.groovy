package dabdata

import grails.core.GrailsApplication
import dabdata.Transmitter;
import dabdata.Multiplex;
import dabdata.BroadcastService;
import dabdata.Party;
import grails.converters.*;
import dabdata.SlugGeneratorService;
import dabdata.PermissionsHelperService;

import grails.plugin.springsecurity.annotation.Secured



class AdminController {

  SlugGeneratorService slugGeneratorService
  PermissionsHelperService permissionsHelperService
  static responseFormats = ['json', 'xml']


  private static SAME_AS_QRY='''
select b from BroadcastService as b 
where exists ( select sa 
               from SameAs as sa 
               where sa.authority=:a 
                 and sa.remoteid=:id
                 and sa.rrType = "BroadcastService"
                 and sa.rrId = b.id )
'''

  private static MULTIPLEX_SAME_AS_QRY='''
select b from Multiplex as b 
where exists ( select sa 
               from SameAs as sa 
               where sa.authority=:a 
                 and sa.remoteid=:id
                 and sa.rrType = "Multiplex"
                 and sa.rrId = b.id )
'''


  @Secured(['ROLE_ADMIN'])
  def processAccessRequest() {

    log.info("processAccessRequest: ${request.method} ${request.JSON} ${getPrincipal()?.username}");

    def result=[
      status:'ERROR',
      message:'Unknown'
    ]

    if ( ( request.method == 'POST' ) && 
         ( request.JSON?.details?.id ) ) {

      AccessRequest.withNewTransaction { ts ->
        def details = request.JSON.details;
        AccessRequest ar = AccessRequest.get(details.id)
        ar.dateActioned = new Date()
        ar.actionedBy = getPrincipal()?.username;
        switch ( request.JSON?.action ) {
          case 'ACCEPT':
            ar.status='ACCEPT'
            ar.reason='Accepted'
            result.status='OK'
            Party p = null;
            if ( details.authorityId != null ) {
              result.message='request accepted - existing authority'
              p = Party.get(details.authorityId);
            }
            else {
              String new_authority_slug = slugGeneratorService.getSlug('dabdata.Party', 'shortcode', details.authorityName)
              result.message='request accepted - new authority: '+details.authorityName+' slug will be '+new_authority_slug
              p = new Party(shortcode: new_authority_slug,
                                 name: details.authorityName).save(flush:true, failOnError:true);
            }

            if ( p != null ) {
              def g = permissionsHelperService.grant(
                       ar.requestedPerm,
                       p.fqshortcode,
                       '%', // Resource type - everything under p.fqshortcode
                       '%', // Resource ID - everything under p.fqshortcode
                       'dabdata.Party',
                       ar.userName);

              // Finally - get a hold of the resource - if it's owner is not yet set then set it to the party created
              // - Essentially the "Adopt" workflow
              switch ( ar.resourceType ) {
                case 'Multiplex':
                  Multiplex mp = Multiplex.get(ar.resourceId);
                  if ( ( mp != null ) && 
                       ( ( mp.owner == null ) || ( mp.owner.fqshortcode == '/ROOT' ) ) ) {
                      mp.owner = p;
                      mp.save(flush:true, failOnError:true);
                  }
                  break;
                default:
                  break;
              }
            }
            break;
          case 'REJECT':
            ar.status='REJECT'
            ar.reason='Rejected'
            result.status='OK'
            result.message='request rejected'
            break;
          default:
            result.message="Unhandled action ${request.JSON?.action}"
            break;
        }
        ar.save(flush:true, failOnError:true);
      }
    }
    else {
      result.status='ERROR'
      result.message='processAccessRequest requires post'
    }

    render result as JSON;
  }

  /**
   * assertData is a way for us to cross reference different authoritative sources. For example
   * OFCOM are the definitive source of information for license status info in the UK, we allow OFCOM
   * to overwrite certain fields on BroadcastServices and allow supplementary fields from OFCOM to be used in
   * the absense of more authoritative data, but we would not allow supplementary fields from OFCOM to
   * overwrite existing data if already present.
   * requests to assertData have for sections: type, match, authorityData and supplementaryData
   * type contains the type of record
   * the match section contains properties used to identify a record
   * authorityData are properties that must be overwritten
   * supplementaryData can be used to create full records but should not overwrite existing data
   */
  @Secured(['ROLE_ADMIN', 'ROLE_DATA_AGENT'])
  public assertData() {
    def record = request.JSON
    def result = null;
    switch ( record?.type ) {
      case 'broadcastService':
        result = assertBroadcastService(record);
        break;
      case 'multiplex':
        result = assertMultiplex(record);
        break;
      default:
        log.warn("Unhandled assertion type: ${record?.type}");
        result = [ 'error': "Unhandled assertion type: ${record?.type}" ]
        break;
    }
    render result as JSON;
  }

  private Map assertMultiplex(Map record) {
    def result = [message:'', log:[]]
    log.info("assertMultiplex ${record}");
    Multiplex mp = null;
    Multiplex.withTransaction { status ->
      mp = matchMultiplex(record)
      if ( ( mp != null ) || ( ( mp == null ) &&  ( record.options?.upsert == true ) ) ) {
        if ( mp == null ) {
          result.message="Created new record"
          mp = new Multiplex(owner: Party.findByShortcode('ROOT'));
          record.match?.each { k, v ->
            result.log.add("[match] Assert ${k} = ${v}");
            mp."${k}" = v
          }
        }
        else {
          result.message="Update multiplex record ${mp.id}";
        }
        record.authorityData?.each { k, v ->
          if ( v instanceof String ) {
            log.debug("Assert ${k} = ${v}");
            result.log.add("[Authoritative] Setting ${k}=${v}");
            mp."${k}" = v
          }
        }
        record.supplementaryData?.each { k, v ->
          if ( v instanceof String ) {
            if ( mp."${k}" == null ) {
              result.log.add("[Supplemental] Setting ${k}=${v}");
              mp."${k}" = v
            }
          }
        }


        mp.save(flush:true, failOnError:true)
        log.debug("Saved multiplex ${mp.id}");

        if ( record.sameAs != null ) {
          assertSameAs('Multiplex',mp.id,record.sameAs);
        }
      }
    }
    return result;
  }

  private Map assertBroadcastService(Map record) {
    def result = [message:'', log:[]]
    log.info("assertBroadcastService ${record}");
    BroadcastService bs = null;
    BroadcastService.withTransaction { status ->

      bs = matchBroadcastService(record)

      if ( ( bs != null ) ||
           ( ( bs == null ) && ( record.options?.upsert == true ) ) ) {

        if ( bs == null ) {
          result.message="Created new record"
          bs = new BroadcastService(owner: Party.findByShortcode('ROOT'));
          record.match?.each { k, v ->
            result.log.add("[match] Assert ${k} = ${v}");
            bs."${k}" = v
          }
          if ( record.supplementaryData?.name ) {
            bs.slug = slugGeneratorService.getSlug('dabdata.BroadcastService', 'slug', record.supplementaryData?.name)
          }
        }
        else {
          result.message="Update broadcast service record ${bs.id}";
        }

        record.authorityData?.each { k, v ->
          if ( v instanceof String ) {
            log.debug("Assert ${k} = ${v}");
            result.log.add("[Authoritative] Setting ${k}=${v}");
            bs."${k}" = v
          }
        }

        record.supplementaryData?.each { k, v ->
          if ( v instanceof String ) {
            if ( bs."${k}" == null ) {
              result.log.add("[Supplemental] Setting ${k}=${v}");
              bs."${k}" = v
            }
          }
        }
        bs.save(flush:true, failOnError:true)

        if ( record.sameAs != null ) {
          assertSameAs('BroadcastService',bs.id,record.sameAs);
        }
      }
      else {
        result.message="No match against supplied criteria ${record.match}";
      }
    }
    return result;
  }

  private void assertSameAs(String type, String id, Map sameAsData) {
    sameAsData.each { k,v ->
      if ( v instanceof List ) {
        v.each { sa ->
          assertSameAs(type, id, k, sa);
        }
      }
      else {
        assertSameAs(type, id, k, v);
      }
    }
  }

  private void assertSameAs(String type, String id, String authority, String value) {

    log.debug("Asserting ${authority}:${value} SAME-AS ${type}:${id}");

    def existingSameAs = SameAs.executeQuery('select sa from SameAs as sa where sa.rrType=:a and sa.rrId=:b and sa.authority=:c and sa.remoteId=:d',
                                             [a:type,b:id,c:authority,d:value]);
    switch ( existingSameAs.size() ) {
      case 0:
        // create
        def newsa = new SameAs(rrType:type, rrId: id, authority: authority, remoteId: value).save(flush:true, failOnError:true);
        break;
      case 1:
        // already present
        break;
      default:
        throw new RuntimeException("Multiple matches for same-as criteria");
        break;
    }
  }

  private Multiplex matchMultiplex(Map record) {
    Multiplex result = null;
    [ 'id', 'ead', 'licenseNumber' ].each { idfield ->
      if ( ( result == null ) && ( record.match.containsKey(idfield) ) ) {
        log.debug("Attempting multiplex match on ${idfield} = ${record.match[idfield]}");
        def qr = Multiplex.executeQuery("select m from Multiplex as m where m.${idfield} = :v".toString(), [v: record.match[idfield]])
        if ( qr?.size() == 1 ) {
          log.info("match on ${idfield}=${record.match[idfield]}");
          result = qr.get(0)
        }
      }
    }

    // Can we match on any same-as assertions
    if ( result == null ) {
      record.match.each { k, v ->
        if ( [ 'id', 'ead', 'licenseNumber' ].contains(k) ) {
          // Don't do anything with the id, ead or licenseNumber
        }
        else {
          def qr = Multiplex.executeQuery(MULTIPLEX_SAME_AS_QRY, [ a: k, id: v ])
          if ( ( qr?.size() == 1 ) && ( result != null ) ) {
            result = qr.get(0);
          }
        }
      }
    }

    return result;
  }

  private BroadcastService matchBroadcastService(Map record) {

    BroadcastService result = null;

    [ 'id', 'fqdn', 'sid', 'license' ].each { idfield ->
      if ( ( result == null ) && ( record.match.containsKey(idfield) ) ) {
        log.debug("Attempting service match on ${idfield} = ${record.match[idfield]}");
        def qr = BroadcastService.executeQuery("select b from BroadcastService as b where b.${idfield} = :v".toString(), [v: record.match[idfield]])
        if ( qr?.size() == 1 ) {
          log.info("match on ${idfield}=${record.match[idfield]}");
          result = qr.get(0)
        }
      }
    }

    // Can we match on any same-as assertions
    if ( result == null ) {
      record.match.each { k, v ->
        if ( [ 'id', 'fqdn', 'sid', 'license' ].contains(k) ) {
          // Don't do anything with the id, fqdn, sid or license propery
        }
        else {
          def qr = BroadcastService.executeQuery(SAME_AS_QRY, [ a: k, id: v ])
          if ( ( qr?.size() == 1 ) && ( result != null ) ) {
            result = qr.get(0);
          }
        }
      }
    }

    return result;
  }


}
