package dabdata

import grails.rest.*
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured
import dabdata.Multiplex;
import grails.orm.PagedResultList
import groovy.util.logging.Slf4j

@Slf4j
class AccessRequestController extends RestfulController<AccessRequest> {

  static responseFormats = ['json', 'xml']

  AccessRequestController() {
    this(false)
  }

  AccessRequestController(boolean readOnly) {
    super(AccessRequest, readOnly)
  }

  @Override
  @Secured('ROLE_ADMIN')
  def index() { 
    Map result = null;
    log.debug("AccessRequestController::index() starting q=${params.q}");

    if ( ( params.q != null ) && ( params.q.length() > 0 ) ) {
      grails.orm.PagedResultList prl = resource.findAllByLuceneQueryString(params.q, params)
      log.debug("AccessRequestController::index() create result");
      if ( prl != null ) {
        result = [ 
          totalCount: prl.totalCount,
          resultList: prl.resultList
        ]
      }
      else {
        result = [ totalCount:0, message:'internal error PRL is null' ]
      }
    }
    else {
      result = [
        totalCount:0,
        message:'no-query-supplied'
      ]
    }

    log.debug("AccessRequestController::index() returning");
    // render result as JSON
    respond result
  }

  @Override
  @Secured('ROLE_ADMIN')
  def show() {
    super.show()
  }

  @Override
  @Secured(['ROLE_ADMIN'])
  def create() {
    super.create()
  }

  @Override
  @Secured(['ROLE_ADMIN'])
  def edit() {
    super.edit()
  }


  @Override
  @Secured(['ROLE_EDITOR', 'ROLE_ADMIN'])
  def save() {
    log.debug("Calling save on ${resource}");
    super.save()
  }
  
  @Override
  @Secured(['ROLE_EDITOR', 'ROLE_ADMIN'])
  def update() {
    super.update()
  }

  @Override
  @Secured('ROLE_ADMIN')
  def delete() {
    super.delete()
  }

}
