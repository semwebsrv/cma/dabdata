package dabdata


import grails.rest.*
import grails.converters.*
import grails.plugin.springsecurity.annotation.Secured
import dabdata.Party;

class PartyController extends DDRestfulController<Party> {

  static responseFormats = ['json', 'xml']

  PartyController() {
    super(Party);
  }
	
  @Override
  protected Party queryForResource(Serializable id) {
    Party result = null;
    result = Party.get(id);
    if ( result == null ) {
      result = Party.findByFqshortcode(id)
    }
    if ( result == null ) {
      result = Party.findByShortcode(id)
    }
    return result;
  }

}
