package dabdata

/**
 * Grant a permission to perform some action against 1 or more resources
 * to 1 or more users.
 *
 * Normally a comain class will provide a customised getDetachedCriteria with the signature
 *    public static DetachedCriteria getDetachedCriteria()
 * which builds in a access to this class in order to determine if a user has the required access
 *
 * In HQL this would be a clause -like-
 * WHERE...
 *    EXISTS (
 *      select g from Grant as g 
 *        where (
 *              :owner like g.grantResourceOwner
 *          and :rclass like g.grantResourceType
 *          and :rid like g.grantResourceId
 *        ) 
 *        and ( 
 *             ( g.granteeType = 'User' and g.granteeId = :userid )                                      - explicit grant to user
 *          OR ( g.granteeType = 'Group' and g.grantResourceId in ( select groups user is a member of ) )    - users groups
 *        )
 *        and :reqperm like g.grantedPerm.permCode 
 *    )
 *
 *  This will match the following use cases
 *  ------ GRANT ON WHAT ---------- ----------- To WHOM ---------------------   - WHAT PERM -
 *  Owner/
 *  Party      Res Type   Res ID    GranteeType              GranteeId          RequiredPerm  Result
 *  %          %          %         User                     AdminUUID          %             Admin can do anything to anything
 *  /Org/Dept1 %          %         Group                    Dept1StaffUUID     %             Users in group Dept1Staff can do anything to Dept1 resources
 *  %          Widget     %         Group                    WidgetAdmins       %             Members of widget admins can do anything to widgets 
 *  %          %          %         Group                    Accounts           Accounts      Members of accounts get all permissions in the Accounts permissions group on everything
 *  /Org       %          %         Group                    OrgAdm             %             Members of OrgAdm can access any resource under Org
 * 
 */

class Grant {

  String id

  String grantResourceOwner

  // -- classname
  String grantResourceType

  // -- id
  String grantResourceId

  // Granted permission
  String grantedPerm

  // class - user or party
  String granteeType

  // id
  String granteeId

  static constraints = {
    grantResourceOwner(nullable:true, blank:false);
     grantResourceType(nullable:false, blank:false);
       grantResourceId(nullable:true, blank:false);
           grantedPerm(nullable:false, blank:false);
           granteeType(nullable:false, blank:false);
             granteeId(nullable:false, blank:false);
  }

  static query_config = [
    defaultField:'id'
  ]


  static belongsTo = [
  ]

  static hasMany = [
  ]

  static mappedBy = [
  ]

  static mapping = {
    table 'user_perm_grant'
    id column:'gr_id', generator: 'uuid2', length:36
    grantResourceOwner column:'gr_granted_resouce_owner', index: 'grant_idx, grant_owner_id, grant_resource_idx'
     grantResourceType column:'gr_granted_resouce_type', index: 'grant_idx, grant_resource_idx'
       grantResourceId column:'gr_granted_resource_id', index: 'grant_idx, grant_resource_idx'
           granteeType column:'gr_grantee_type', index: 'grant_idx, grantee_idx'
             granteeId column:'gr_grantee_id', index: 'grant_idx, grantee_idx'
           grantedPerm column:'gr_granted_perm', index: 'grant_idx'
  }

}
