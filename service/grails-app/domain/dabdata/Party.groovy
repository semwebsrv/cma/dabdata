package dabdata


/**
 * A party who will participate in some kind of formal agreement. Parties are normally
 * Organisations or individuals. Party may be used to attach policy or access rules
 * - IE a party might be an local library authority which acts as an identity provider
 * for patrons and the legal entity who manages branch libraries. All libraries will
 * fall under the umbrella of the LA party. users affiliated with that LA will likely be
 * able to to access secured services.
 *
 * PARTY Is a SYNONYM for "OrganisationalUnit"- which would have been a better name in retrospect
 *
 * N.B. parent, shortcode and fqshortcode are NOT updatable - once set they are set for life
 * too much system behaviour depends on these things being stable.
 *
 * for ref https://gitlab.com/ianibbo/holocore/-/blob/269a6932072d52579fa72262d50e6708273aa744/grails-app/domain/holo/core/parties/Party.groovy
 */
class Party {

  String id
  String shortcode
  String fqshortcode
  String name

  // The "Authoritative" domain - EG sheffield.gov.uk, someuni.edu, someuni.ac.uk, somecompany.com
  // This will be used in combination with authPath to obtain an authority file that a user can use to prove
  // that they are approved by the party for some level of grant
  String authDomain
  // The path at authDomain ON SSH where an authority file that contains, amongst other things, a public key
  String authPath

  static constraints = {
             name (nullable:false, blank:false, unique:true)
        shortcode (nullable:false, blank:false, unique:true);
      fqshortcode (nullable:true, blank:false, unique:true);
           parent (nullable:true);
       authDomain (nullable:true, blank:false);
         authPath (nullable:true, blank:false);
  }

  static mapping = {
               id column:'pa_id', generator: 'uuid2', length:36
             name column:'pa_name'
           parent column:'pa_parent_fk'
        shortcode column:'pa_shortcode'
      fqshortcode column:'pa_fqshortcode', updateable: false
       authDomain column:'pa_authdomain'
         authPath column:'pa_authpath'
  }

  static query_config = [
    defaultField:'name',
    properties:[
      name:[mode:'keyword']
    ]
  ]

  static belongsTo = [
    parent: Party
  ]

  static hasMany = [
    units: Party
  ]

  static mappedBy = [
    units:'parent'
  ]

  def beforeInsert() {
    // If we are not explicitly given a shortcode and a parent is set
    // then construct a shortcode
    if ( ( shortcode != 'ROOT' ) && ( parent == null ) ) {
      parent = Party.findByShortcode('ROOT')
    }

    if ( fqshortcode == null ) {
      if ( parent != null ) {
        fqshortcode = parent.fqshortcode+'.'+shortcode
      }
      else {
        fqshortcode = shortcode
      }
    }
  }

}
