package dabdata

import java.time.LocalDate


/**
 * Bearer.
 * A Bearer can be of type DAB or FM
 * - When a bearer is of type DAB
 *   - it notionally an ensemble.broadcastService
 *   - it belongs to a multiplex
 * - wheras an FM bearer
 *   - has many transmitters (Which can be linked to many FM bearers)
 *
 * A bearer represents the relationship between a Broadcast Service and the infrastructure which bears that service
 *  The infrastructure could be a multiplex(DAB)  or an FM transmitter depending upon it's type
 *
 */
class Bearer { // implements grails.plugins.orm.auditable.Auditable {

  String id
  String type // DAB or FM or Web
  BroadcastService service
  // Long startTimestamp
  // Long endTimestamp

  LocalDate startDate
  LocalDate endDate

  // Type:DAB properties
  Multiplex multiplex
 
  // Type:FM properties
  Set fmTransmitters

  String streamingWebAddress

  // DAB Specific properties
  String capacityUnits
  String mode
  String protection
  String parity
  String samplingRate
  Long bitRateKbps

  static constraints = {
                multiplex (nullable:true)
                  service (nullable:false)
                     type (nullable:false, blank:false)
      streamingWebAddress (nullable:true)

                startDate (nullable:true)
                  endDate (nullable:true)

            capacityUnits (nullable:true)
                     mode (nullable:true)
               protection (nullable:true)
                   parity (nullable:true)
             samplingRate (nullable:true)
              bitRateKbps (nullable:true)
  }

  static hasMany = [
    fmTransmitters:FMBearerTransmitter,
  ]

  static mappedBy = [
    fmTransmitters:'bearer'
  ]

  static query_config = [
    defaultField:'service.name',
    properties:[
      name:[mode:'keyword']
    ]
  ]

  static mapping = {
                     id column: 'be_id', generator: 'uuid2', length:36
                   type column: 'be_type'
                service column: 'be_service_fk'
              multiplex column: 'be_multiplex_fk'

              startDate column: 'be_start_date'
                endDate column: 'be_end_date'

    streamingWebAddress column: 'be_streaming_web_addr'

          capacityUnits column: 'be_cu'
                   mode column: 'be_mode'
             protection column: 'be_prot'
                 parity column: 'be_parity'
           samplingRate column: 'be_samplingRate'
            bitRateKbps column: 'be_bitrate_kbps'
  }

}
