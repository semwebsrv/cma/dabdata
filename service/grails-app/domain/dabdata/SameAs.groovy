package dabdata


/**
 */
class SameAs {

  String id
  String rrType
  String rrId
  String authority
  String remoteId

  static constraints = {
             rrType (nullable:false, blank:false);
               rrId (nullable:false, blank:false);
          authority (nullable:false, blank:false);
           remoteId (nullable:false, blank:false);
  }

  static mapping = {
            id column:'sa_id', generator: 'uuid2', length:36
        rrType column:'sa_rr_type'
          rrId column:'sa_rr_id'
     authority column:'sa_authority'
      remoteId column:'sa_remoteId'
  }

}
