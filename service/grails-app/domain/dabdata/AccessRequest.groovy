package dabdata

class AccessRequest {

  String id
  String userName      // ian.ibbotson@semweb.co
  String authorityName // BillDAB CIC
  String authorityId   // BillDAB CIC
  String requestedPerm // Editor
  String resourceType  // Transmitter
  String resourceId    // 1234-5678
  String resourceLabel // transmitter-name
  String notes
  Date dateOfRequest
  String actionedBy
  Date dateActioned
  String status
  String reason


  static query_config = [
    defaultField:'userName',
    properties:[
      userName:[mode:'keyword'] // Wildcard translation only works on keyword fields
    ]
  ]

  
  static constraints = {
         userName(nullable:false, blank:false);
    authorityName(nullable:true, blank:false);  // If the authority already exists, set id below
      authorityId(nullable:true, blank:false);  // If the authority does not exist, set it's name above
    requestedPerm(nullable:false, blank:false);
     resourceType(nullable:false, blank:false);
       resourceId(nullable:false, blank:false);
    resourceLabel(nullable:false, blank:false);
            notes(nullable:true);
    dateOfRequest(nullable:false);
     dateActioned(nullable:true);
       actionedBy(nullable:true, blank:false);
           status(nullable:true, blank:false);
           reason(nullable:true, blank:false);
  }

  static mapping = {
    table 'access_request'
    id column:'ar_id', generator: 'uuid2', length:36
          userName column:'ar_user_name'
     authorityName column:'ar_authority_name'
       authorityId column:'ar_authority_id'
     requestedPerm column:'ar_requested_perm'
      resourceType column:'ar_resource_type'
        resourceId column:'ar_resource_id'
     resourceLabel column:'ar_resource_label'
             notes column:'ar_notes'
     dateOfRequest column:'ar_date_of_request'
      dateActioned column:'ar_date_actioned'
        actionedBy column:'ar_actionedby'
            status column:'ar_status'
            reason column:'ar_reason'
  }

  public String toString() {
    return "AccessRequest ${userName} ${authorityName} ${authorityId} ${requestedPerm}".toString();
  }
}
