package dabdata

class BroadcastServiceStream {

  String id
  String name
  String url
  Boolean isPublic
  String description
  String usage
  BroadcastService owner

  static constraints = {
           name (nullable:true, blank:false)
            url (nullable:false, blank:false)
       isPublic (nullable:true)
    description (nullable:true)
          usage (nullable:true)
  }

  static belogsTo = [
    owner: BroadcastService
  ]

  static mapping = {
             id column: 'bss_id', generator: 'uuid2', length:36
           name column: 'bss_name'
            url column: 'bss_url'
       isPublic column: 'bss_is_public'
    description column: 'bss_description'
          usage column: 'bss_usage'
  }

}
