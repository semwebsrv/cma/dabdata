package dabdata

import com.ibm.icu.text.Normalizer2

class RefdataValue {
  
  private static final Normalizer2 normalizer = Normalizer2.NFKDInstance

  String id
  String value
  String label
  String order

  static belongsTo = [
    owner:RefdataCategory
  ]

  static mapping = {
    id column: 'rdv_id', generator: 'uuid', length:36
    version column: 'rdv_version'
    owner column: 'rdv_owner', index:'rdv_entry_idx'
    value column: 'rdv_value', index:'rdv_entry_idx'
    label column: 'rdv_label'
    order column: 'rdv_order_value'
  }

  static constraints = {
    label (nullable: false, blank: false)
    value (nullable: false, blank: false)
    owner (nullable: false)
    order (nullable: true)
  }
  
  public static String normValue ( String string ) {
    // Remove all diacritics and substitute for compatibility
    normalizer.normalize( string.trim() ).replaceAll(/\p{M}/, '').replaceAll(/\s+/, '_').toLowerCase()
  }
  
  private static String tidyLabel ( String string ) {
    string.trim().replaceAll(/\s{2,}/, ' ')
  }
  
  void setValue (String value) {
    this.value = normValue( value )
  }
  
  void setLabel (String label) {
    this.label = tidyLabel( label )
    if (this.value == null) {
      this.setValue( label )
    }
  }
  
  /**
   * Lookup or create a RefdataValue
   * @param category_name
   * @param value
   * @return
   */
  static RefdataValue lookupOrCreate(final String category_name, final String label, final String value=null, final String order=null) {
    RefdataCategory cat = RefdataCategory.findByDesc(category_name)
    if (!cat) {
      cat = new RefdataCategory()
      cat.desc = category_name
      cat.save(flush:true, failOnError:true)
    }
    
    lookupOrCreate (cat, label, value, order)
  }
  
  static RefdataValue lookupOrCreate(final RefdataCategory cat, final String label, final String value=null, final String order=null) {
    
    final String norm_value = normValue( value ?: label )
    
    RefdataValue result = RefdataValue.findByOwnerAndValue(cat, norm_value) 
    
    if (!result) {
      result = new RefdataValue()
      result.label = label
      result.value = norm_value
      result.owner = cat
      result.order = order
      result.save(flush:true, failOnError:true)
    }
    result
  }

}
