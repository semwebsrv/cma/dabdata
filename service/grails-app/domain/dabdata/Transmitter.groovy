package dabdata

class Transmitter { // implements grails.plugins.orm.auditable.Auditable {

  String id
  String name
  String normname
  String slug
  Set multiplexes
  Set fmBearers
  Party owner
  Double lat
  Double lon
  Double siteHeight
  String lcsRef
  String transmitterUrl
  String regulatoryBodyUrl
  String description
  Date dateCreated
  Date lastUpdated

  static constraints = {
                 name (nullable:false, blank:false)
             normname (nullable:true, blank:false, unique:true)
                owner (nullable:false)
                 slug (nullable:true, blank:false, unique:true)
                  lat (nullable:true)
                  lon (nullable:true)
           siteHeight (nullable:true)
               lcsRef (nullable:true)
       transmitterUrl (nullable:true)
    regulatoryBodyUrl (nullable:true)
          description (nullable:true)
          dateCreated (nullable:true)
          lastUpdated (nullable:true)
  }

  static hasMany = [
    fmBearers: FMBearerTransmitter,
    multiplexes: MultiplexTransmitter
  ]

  static mappedBy = [
    multiplexes: 'transmitter',
    fmBearers: 'transmitter'
  ]

  static query_config = [
    defaultField:'name',
    properties:[
      name:[mode:'keyword']
    ]
  ]

  static mapping = {
                   id column: 'tr_id', generator: 'uuid2', length:36
                 name column: 'tr_name'
             normname column: 'tr_normalised_name'
                owner column: 'tr_owner_fk'
                 slug column: 'tr_slug'
                  lat column: 'tr_lat'
                  lon column: 'tr_lon'
           siteHeight column: 'tr_site_height'
               lcsRef column: 'tr_lcs_ref'
       transmitterUrl column: 'tr_url'
    regulatoryBodyUrl column: 'tr_reg_url'
          description column: 'tr_description', type:'text'
          dateCreated column: 'tr_date_created'
          lastUpdated column: 'tr_last_updated'
  }

  def beforeInsert() {
    if ( normname == null ) {
      normname = name?.toLowerCase().trim().replaceAll("\\p{Punct}", "");
    }
    if ( slug == null ) {
      slug = normname.replaceAll(' ','_')
    }
  }

  def beforeUpdate() {
    def new_normname = name?.toLowerCase().trim().replaceAll("\\p{Punct}", "");
    if ( new_normname != normname )
      normname = new_normname;
  }

}
