package dabdata

import java.time.LocalDate

class Multiplex implements grails.plugins.orm.auditable.Auditable {

  String id
  String name
  String ead // Assigned Ensemble ID
  String area
  String block
  String frequency
  String homepage
  Party owner
  String description
  String logoUrl

  // classn will be used to store multiplex type - SmallScale, etc.
  String classn

  String licenseNumber
  String licensee
  String licenseeWebsite

  String addrLine1
  String addrLine2
  String addrTown
  String addrPostcode
  String tel
  String emailAddress
  String twitterHandle
  String fbPage
  String instagramAddr
  String youtubeChannel

  LocalDate launchDate
  Date dateCreated
  Date lastUpdated
  String lastUpdatedBy

  String logo32
  String logo128

  RefdataValue region

  static constraints = {
                name (nullable:false, blank:false)
                 ead (nullable:true, blank:false, unique:true)
                area (nullable:true, blank:false)
               block (nullable:true, blank:false)
           frequency (nullable:true, blank:false)
       licenseNumber (nullable:true, blank:false)
            homepage (nullable:true, blank:false)
               owner (nullable:false)
         description (nullable:true, blank:false)

            licensee (nullable:true, blank:false)
     licenseeWebsite (nullable:true, blank:false)
           addrLine1 (nullable:true, blank:false)
           addrLine2 (nullable:true, blank:false)
            addrTown (nullable:true, blank:false)
        addrPostcode (nullable:true, blank:false)
                 tel (nullable:true, blank:false)
        emailAddress (nullable:true, blank:false)
          launchDate (nullable:true)

       twitterHandle (nullable:true, blank:false)
              fbPage (nullable:true, blank:false)
       instagramAddr (nullable:true, blank:false)
      youtubeChannel (nullable:true, blank:false)
             logoUrl (nullable:true, blank:false)
              classn (nullable:true, blank:false)
         dateCreated (nullable:true)
         lastUpdated (nullable:true)
       lastUpdatedBy (nullable:true)

            logo32 (nullable:true, blank:false)
           logo128 (nullable:true, blank:false)

            region (nullable:true)


  }

  static mapping = {
                  id column: 'mp_id', generator: 'uuid2', length:36
                name column: 'mp_name'
                 ead column: 'mp_assigned_ead' // Assigned 
                area column: 'mp_area'
               block column: 'mp_block'
           frequency column: 'mp_frequency'
       licenseNumber column: 'mp_license_number'
            homepage column: 'mp_homepage'
               owner column: 'mp_owner_fk'
         description column: 'mp_description', type:'text'

            licensee column: 'mp_licensee'
     licenseeWebsite column: 'mp_licensee_website'
           addrLine1 column: 'mp_addr_line_1'
           addrLine2 column: 'mp_addr_line_2'
            addrTown column: 'mp_addr_town'
        addrPostcode column: 'mp_addr_postcode'
                 tel column: 'mp_tel'
        emailAddress column: 'mp_email_addr'
          launchDate column: 'mp_launch_date'

       twitterHandle column: 'mp_twitter'
              fbPage column: 'mp_fb_page'
       instagramAddr column: 'mp_instagram'
      youtubeChannel column: 'mp_youtube'
              classn column: 'mp_classn'

             logoUrl column: 'mp_logo_url'
              logo32 column: 'mp_logo_32'
             logo128 column: 'mp_logo_128'

         dateCreated column: 'mp_date_created'
         lastUpdated column: 'mp_last_updated'

              region column: 'mp_region_fk'
  }

  static hasMany = [
    bearers: Bearer,
    transmitters: MultiplexTransmitter
  ]

  static mappedBy = [
    bearers: 'multiplex',
    transmitters: 'multiplex'
  ]

  static query_config = [
    defaultField:'name',
    properties:[
      name:[mode:'keyword']
    ]
  ]

  static transients = ['sortedBearers']

  public List<Bearer> getSortedBearers() {
    return Bearer.executeQuery('select b from Bearer as b where b.multiplex = :m order by b.service.name',[m:this])
  }
}
