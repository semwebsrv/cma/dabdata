package dabdata

class BroadcastService { // implements grails.plugins.orm.auditable.Auditable {

  String id
  String name
  String normname
  String slug
  String fqdn
  String sid
  String lsn
  Party owner
  String description
  String stationUrl
  String regulatoryUrl
  RefdataValue status
  String authority

  String license
  String licensee

  String bitrate
  String codec
  String channelType
  String hasSlideshow
  String genre
 
  String addrLine1
  String addrLine2
  String addrTown
  String addrPostcode
  String tel
  String emailAddress

  String twitterHandle
  String fbPage
  String instagramAddr
  String youtubeChannel

  String logoUrl
  String logo32
  String logo128

  Date dateCreated
  Date lastUpdated


  static constraints = {
              name (nullable:false, blank:false)
          normname (nullable:true, blank:false)
              slug (nullable:true, blank:false)
              fqdn (nullable:true, blank:false)
               sid (nullable:true, blank:false)
               lsn (nullable:true, blank:false)
             owner (nullable:false)
       description (nullable:true, blank:false)
        stationUrl (nullable:true, blank:false)
     regulatoryUrl (nullable:true, blank:false)
            status (nullable:true )
         authority (nullable:true, blank: false )
           license (nullable:true, blank: false )
          licensee (nullable:true, blank: false )

           bitrate (nullable:true, blank: false )
             codec (nullable:true, blank: false )
       channelType (nullable:true, blank: false )
      hasSlideshow (nullable:true, blank: false )
             genre (nullable:true, blank: false )

         addrLine1 (nullable:true, blank:false)
         addrLine2 (nullable:true, blank:false)
          addrTown (nullable:true, blank:false)
      addrPostcode (nullable:true, blank:false)
               tel (nullable:true, blank:false)
      emailAddress (nullable:true, blank:false)

     twitterHandle (nullable:true, blank:false)
            fbPage (nullable:true, blank:false)
     instagramAddr (nullable:true, blank:false)
    youtubeChannel (nullable:true, blank:false)
           logoUrl (nullable:true, blank:false)
            logo32 (nullable:true, blank:false)
           logo128 (nullable:true, blank:false)

       dateCreated (nullable:true, blank:false)
       lastUpdated (nullable:true, blank:false)

  }

  static hasMany = [
    bearers: Bearer,
    streams: BroadcastServiceStream
  ]

  static mappedBy = [
    bearers: 'service',
    streams: 'owner'
  ]

  static query_config = [
    defaultField:'name',
    properties:[
      name:[mode:'keyword']
    ]
  ]

  static transients = ['sameAs']

  static mapping = {
               id column: 'bs_id', generator: 'uuid2', length:36
             name column: 'bs_name'
             slug column: 'bs_slug'
             fqdn column: 'bs_fqdn'
              sid column: 'bs_sid'
              lsn column: 'bs_lsn'
            owner column: 'bs_owner_fk'
      description column: 'bs_description', type:'text'
       stationUrl column: 'bs_station_url'
    regulatoryUrl column: 'bs_regulatory_url'
           status column: 'bs_status_fk'
        authority column: 'bs_authority'

        addrLine1 column: 'bs_addr_line_1'
        addrLine2 column: 'bs_addr_line_2'
         addrTown column: 'bs_addr_town'
     addrPostcode column: 'bs_addr_postcode'
              tel column: 'bs_tel'
     emailAddress column: 'bs_email_addr'

          bitrate column: 'bs_bitrate'
            codec column: 'bs_codec'
      channelType column: 'bs_channel_type'
     hasSlideshow column: 'bs_has_slideshow'
            genre column: 'bs_genre'

    twitterHandle column: 'bs_twitter'
           fbPage column: 'bs_fb_page'
    instagramAddr column: 'bs_instagram'
   youtubeChannel column: 'bs_youtube'
          logoUrl column: 'bs_logo_url'
           logo32 column: 'bs_logo_32'
          logo128 column: 'bs_logo_128'
      dateCreated column: 'bs_date_created'
      lastUpdated column: 'bs_last_updated'
  }

  def beforeInsert() {
    if ( normname == null ) {
      normname = name?.toLowerCase().trim().replaceAll("\\p{Punct}", "");
    }
    if ( slug == null ) {
      slug = normname.replaceAll(' ','_')
    }
  }

  def beforeUpdate() {
    def new_normname = name?.toLowerCase().trim().replaceAll("\\p{Punct}", "");
    if ( new_normname != normname )
      normname = new_normname;
  }

  public List getSameAs() {
    return SameAs.executeQuery('select sa.authority, sa.remoteId from SameAs as sa where sa.rrType="BroadcastService" and sa.rrId=:id',[id: this.id]);
  }
}
