package dabdata

class MultiplexTransmitter {

  String id
  Multiplex multiplex
  Transmitter transmitter

  static constraints = {
  }

  static hasMany = [
  ]

  static mapping = {
             id column: 'mt_id', generator: 'uuid2', length:36
       multiplex column: 'mt_mp_id'
     transmitter column: 'mt_tr_id'
  }

}
