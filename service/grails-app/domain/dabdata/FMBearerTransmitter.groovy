package dabdata

class FMBearerTransmitter {

  String id
  Bearer bearer
  Transmitter transmitter

  static constraints = {
  }

  static hasMany = [
  ]

  static mapping = {
              id column: 'fbmt_id', generator: 'uuid2', length:36
          bearer column: 'fbmt_be_id'
     transmitter column: 'fbmt_tr_id'
  }

}
