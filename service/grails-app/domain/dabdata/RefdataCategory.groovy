package dabdata

class RefdataCategory {
  
  String id
  String desc
  Set<RefdataValue> values
  
  static hasMany = [
    values: RefdataValue
  ]
  
  static mappedBy = [
    values: 'owner'
  ]

  static mapping = {
         id column: 'rdc_id', generator: 'uuid', length:36
    version column: 'rdc_version'
       desc column: 'rdc_description'
     values cascade: 'all-delete-orphan'
  }
}
