package dabdata

import grails.gorm.transactions.Transactional
import dabdata.Grant;

@Transactional
class PermissionsHelperService {

  public List getUserPermissions(String principal, String resource) {
    log.debug("PermissionsHelperService::getUserPermissions(${principal},${resource})");
    return [
      'one',
      'two'
    ]
  }

  public Grant grant(String perm,
                       String party,
                       String resourceType,
                       String resourceId,
                       String granteeType,
                       String granteeId) {

    Grant g = new Grant(
      grantedPerm: perm,
      grantResourceOwner: party,
      grantResourceType: resourceType,
      grantResourceId: resourceId,
      granteeType: granteeType,
      granteeId: granteeId).save(flush:true, failOnError:true);
    return g;
  }

}
