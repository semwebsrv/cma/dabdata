package dabdata

import grails.gorm.transactions.Transactional
import grails.core.GrailsClass

@Transactional
class SameAsService {

  def grailsApplication;

  public void assertSameAs(String rrType, String rrId, String authority, String remoteId) {

    def qry = SameAs.createCriteria()
    def matches = qry.list {
      eq('authority',authority)
      eq('remoteId',remoteId)
      eq('rrType',rrType)
      eq('rrId',rrId)
    }

    if( matches.size()  == 0 ) {
      def newsa = new SameAs(authority: authority, 
                             remoteId: remoteId,
                             rrType: rrType,
                             rrId: rrId).save(flush:true, failOnError:error);
    }

  }

}


