package dabdata

import java.net.URL
import grails.gorm.transactions.Transactional
import org.apache.commons.io.input.BOMInputStream
import com.opencsv.ICSVParser
import com.opencsv.CSVParser
import com.opencsv.CSVParserBuilder
import com.opencsv.CSVReader
import com.opencsv.CSVReaderBuilder

@Transactional
class ETLHelperService {

  SlugGeneratorService slugGeneratorService
  public static final int NAT_GRID_REF_COL = 137;

  boolean validateURL(String url) {
    log.debug("ETLHelperService::ingestURL(${url})");
    return true
  }

  Map ingestURL(String url) {

    Map result = [
      rows_processed:0,
      multiplexes_created:0,
      multiplexes_updated:0,
      transmitters_created:0,
      transmitters_updated:0,
      multiplex_transmitters_created:0,
      multiplex_transmitters_updated:0,
      broadcast_services_created:0,
      broadcast_services_updated:0,
      broadcast_service_errors:0,
      bearers_created:0,
      bearers_updated:0,
      bearer_errors:0,
      errors:0,
    ]

    log.debug("ETLHelperService::ingestURL(${url})");
    URL input_url = new URL(url);
    BOMInputStream bis = new BOMInputStream(input_url.openStream());
    Reader fr = new InputStreamReader(bis);
    CSVParser parser = new CSVParserBuilder().withSeparator(',' as char)
        .withQuoteChar(ICSVParser.DEFAULT_QUOTE_CHARACTER)
        .withEscapeChar(ICSVParser.DEFAULT_ESCAPE_CHARACTER)
      .build();

    CSVReader csvReader = new CSVReaderBuilder(fr).withCSVParser(parser).build();
    String[] header = csvReader.readNext()
    log.debug("Read header - [${header.length}] values : ${header}");

    if ( ( header.length >= 181 ) &&
         ( header[0].equals('Date') ) &&
         ( header[4].equals('EID') ) && 
         ( header[180].equals('Long') ) ) {

      String[] datarow = csvReader.readNext()
      int rowctr=0;
      // Each row in the csv is a represents the appearence of an ensemble at a transmitter.
      // This is our "Bearer" object - a transmitter "Carries" an ensemble or multiplex and each multiplex can be carried
      // by multiple transmitters.
      while (datarow != null) {
        if ( datarow[2]?.startsWith('SSDAB') ) {
        
          log.debug("--> Process data row ${result.rows_processed++} / ${datarow.length} values");
          
          // datarow[2] should contain the license ID. SSDAB licenses begin SSDAB
          if ( datarow.length > 0 ) {
            // log.debug("Process multiplex ${datarow[4]}");
            String multiplex_ead = datarow[4]?.trim()
            String multiplex_id = lookupOrCreateMultiplex(multiplex_ead, datarow[1], datarow[3], datarow[2], result);
            Double lat = datarow[179] != null ? Double.parseDouble(datarow[179]) : null;
            Double lon = datarow[180] != null ? Double.parseDouble(datarow[180]) : null;
            Double site_height = datarow[139] != null ? Double.parseDouble(datarow[139]) : null;
            // Columns 143 to 178 are 36 10 degree arcs that have coverage information
            Double[] coverage_info = new Double[36]
            int coverage_ctr = 0;
            (143..178).each { arc ->
              if ( ( datarow[arc] != null ) &&
                   ( datarow[arc].length() > 0 ) ) {
                try {
                  coverage_info[coverage_ctr++] = Double.parseDouble(datarow[arc])
                }
                catch ( Exception e ) {
                  log.warn("Unable to parse coverage value row ${rowctr}[${arc}]:\"${datarow[arc]}\"");
                }
              }
            }
            String transmiiter_id = lookupOrCreateTransmitter(datarow[6], lat, lon, site_height, coverage_info, 'OSGB36:'+datarow[NAT_GRID_REF_COL], result)
  
            String mt_id = lookupOrCreateMultiplexTransmitter(multiplex_id, transmiiter_id, result);
    
            // Each row defines up to 32 broadcast services being bourne by this multiplex on this transmitter. they start at column 
            // 11 and appear in blocks of three
            for ( int i=0; i<32; i++ ) {
              String service_label = datarow[11+(i*3)]
              String sid = datarow[12+(i*3)]
              String local_service_number = datarow[13+(i*3)]
              if ( ( sid != null ) && ( service_label != null ) && ( sid.length() > 0 ) && ( service_label.length() > 0 ) ) { 
                String service_id = lookupOrCreateBroadcastService(sid, service_label, local_service_number, 'audio', result)
                if ( service_id ) {
                  assertDABBearer(service_id,multiplex_id, result);
                }
                else {
                  log.error("Failed to locate service ${service_label}.");
                  result.errors++;
                  result.broadcast_service_errors++;
                }
              }
            }
    
            // Each row also identifies up to 16 data services using same pattern, starting at column 11+96=107
            for ( int i=0; i<15; i++ ) {
              String service_label = datarow[107+(i*2)]
              String sid = datarow[108+(i*2)]
              // log.debug("Consider data service [${i}/${108+(i*2)}] ${service_label} / ${sid}");
              if ( ( sid != null ) && ( service_label != null ) && ( sid.length() > 0 ) && ( service_label.length() > 0 ) ) {
                String service_id = lookupOrCreateBroadcastService(sid, service_label, null, 'data', result)
                if ( service_id ) {
                  assertDABBearer(service_id,multiplex_id, result);
                }
                else{
                  log.error("Failed to locate service ${service_label}.");
                  result.errors++;
                  result.broadcast_service_errors++;
                }
              }
            }
          }
        }
        else {
          log.debug("Skip non SSDAB row");
        }
        rowctr++;
        datarow = csvReader.readNext()
      }
    }
    else {
      result.status='ERROR'
      result.message='csv file does not match expected format'
      if ( header.length >= 181 )
        result.info="Expected  columns 0,4 and 180 to be Date,EID and Long - got ${header[0]} ${header[4]} ${header[180]}"
      else
        result.info="Expected at least 181 columns, got ${header.length}"
      result.header=header
    }

    log.debug("Result of ingest: ${result}");
    return result;
  }

  private String lookupOrCreateMultiplex(String ensemble_id, String name, String area, String license_id, Map state) {

    String result = null;

    Multiplex.withNewTransaction { status ->
      // If we have an ensemble id and a name
      if ( ( ensemble_id != null ) && ( name != null ) ) {
        // Lookup the multiplex
        Multiplex multiplex = Multiplex.findByEad(ensemble_id)
        if ( multiplex != null ) {
          log.debug("    -> update multiplex ${ensemble_id}/${name}");
          state.multiplexes_updated++;
        }
        else {
          log.debug("    -> create multiplex ${ensemble_id}/${name}");
          multiplex = new Multiplex(
                                ead:ensemble_id,
                                name:name,
                                area:area,
                                licenseNumber:license_id,
                                owner: Party.findByShortcode('ROOT')
                              ).save()
          state.multiplexes_created++;
        }

        result = multiplex.id;
        log.debug("Returning multiplex id ${result}/${result}");
      }
    }

    return result
  }

  String lookupOrCreateTransmitter(String transmitter_name, 
                                   Double lat, 
                                   Double lon, 
                                   Double site_height, 
                                   Double[] coverage_info, 
                                   String local_coordinate_system_reference,
                                   Map state) {

    String result = null;

    Transmitter.withNewTransaction { status ->
      // Transmitters - Keyed off column g - "Site"
      // Each transmitter carries its own transmitter identification (TII)
      // We combine columns J+K/TII Main Id (Hex)+TII Sub Id (Hex)/9+10 to give us a unique key for the transmitter site
      String transmitter_normalised_name = transmitter_name.toLowerCase().replaceAll("\\p{Punct}", "").trim().replaceAll(" +", " ");

      Transmitter tr = Transmitter.findByNormname(transmitter_normalised_name)
      if ( tr != null ) {
        log.debug("    -> update transmitter ${transmitter_normalised_name}/${transmitter_name}");
        if ( updateField(tr, 'lat', lat) ||
             updateField(tr, 'lon', lon) ||
             updateField(tr, 'siteHeight', site_height) ||
             updateField(tr, 'lcsRef', local_coordinate_system_reference) ) {
          log.debug("        -> Field change detected");
          tr.save(flush:true, failOnError:true)
          state.transmitters_updated++;
        }
      }
      else {
        log.debug("    -> create transmitter ${transmitter_normalised_name}/${transmitter_name}");
        tr = new Transmitter(name: transmitter_name,
                             normname: transmitter_normalised_name,
                             owner: Party.findByShortcode('ROOT'),
                             slug: transmitter_normalised_name.replaceAll(' ','_'),
                             lat: lat,
                             lon: lon,
                             siteHeight: site_height,
                             lcsRef: local_coordinate_system_reference).save(flush:true, failOnError:true)
        state.transmitters_created++;
      }

      result = tr.id
    }

    return result;
  }

  String lookupOrCreateMultiplexTransmitter(multiplex_id, transmiiter_id, Map state) {
    String result = null;
    Bearer.withNewTransaction { status ->
      Transmitter t = Transmitter.get(transmiiter_id)
      Multiplex m = Multiplex.get(multiplex_id)
      if ( t != null && m != null ) {
        MultiplexTransmitter mt = MultiplexTransmitter.findByTransmitterAndMultiplex(t,m);
        if ( mt != null ) {
          log.debug("Transmitter ${transmiiter_id} already associated with multiplex ${multiplex_id}");
          state.multiplex_transmitters_updated++;
        }
        else {
          log.debug("Associating Transmitter ${transmiiter_id} with multiplex ${multiplex_id}");
          mt = new MultiplexTransmitter(transmitter:t, multiplex:m).save(flush:true, failOnError:true);
          state.multiplex_transmitters_created++;
        }
        result = mt.id;
      }
      else {
        log.error("Transmitter(${transmiiter_id}=${t}) or Multiplex(${multiplex_id}=${m}) was null");
      }
    }
    return result;
  }

  String lookupOrCreateBroadcastService(String service_id, String service_label, String localServiceId, String service_type, Map state) {
    log.debug("lookupOrCreateBroadcastService(${service_id},${service_label},${localServiceId},${service_type}...)");
    String result = null;
    BroadcastService.withNewTransaction { status ->
      BroadcastService bs = BroadcastService.findBySid(service_id);
      if ( bs != null ) {
        state.broadcast_services_updated++;
      }
      else {
        bs = new BroadcastService(name:service_label, 
                                  slug: slugGeneratorService.getSlug('dabdata.BroadcastService', 'slug', service_label),
                                  sid:service_id, 
                                  lsn: localServiceId,
                                  owner: Party.findByShortcode('ROOT')).save(flush:true, failOnError:true);
        state.broadcast_services_created++;
      }
      result = bs.id;
    }

    return result;
  }

  String assertDABBearer(String broadcast_service, String multiplex, Map stats) {
    log.debug("assertDABBearer(${broadcast_service}, ${multiplex})");

    String result = null;
    Bearer.withNewTransaction { service ->
      List<Bearer> bqr = Bearer.executeQuery('select b from Bearer as b where b.service.id = :bsid and b.multiplex.id = :mid',
                                             [bsid:broadcast_service, mid:multiplex])
      if ( bqr.size() == 1 ) {
        result = bqr[0].id
        stats.bearers_updated++;
      }
      else if ( bqr.size() == 0 ) {
        BroadcastService svc = BroadcastService.get(broadcast_service)
        Multiplex mp = Multiplex.get(multiplex); 
        if ( ( svc != null ) && ( mp != null ) ) {
          Bearer b = new Bearer( service: svc,
                                 multiplex: mp,
                                 type:'DAB',
                                 startDate: java.time.LocalDate.now() ).save(flush:true, failOnError:true);
          stats.bearers_created++;
          result = b.id
        }
        else {
          stats.bearer_errors++;
          stats.errors++;
        }
      }
      else {
        log.error("Found too many bearer records");
        stats.bearer_errors++;
        stats.errors++;
      }
    }
    return result;
  }

  private boolean updateField(Object o, String fieldname, Object value) {
    boolean result = false;
    try {
      // log.debug("updateField(${fieldname} ${o[fieldname]} ${value})");
      if ( o[fieldname] != value ) {
        o[fieldname] = value;
        result = true;
      }
    }
    catch ( Exception e ) {
      log.error("Problem updating record",e);
    }
    return result;
  }
}
