package dabdata

import grails.gorm.transactions.Transactional
import grails.core.GrailsClass

@Transactional
class SlugGeneratorService {

  def grailsApplication;

  String getSlug(String domain_class_name, String slugField, String label) {

    String base_string = null;
    String result = null;

    if ( ( label == null ) || 
         ( label.replaceAll('\\p{Punct}', '').replaceAll(' +', '').trim().length() == 0 ) ) {
      throw new RuntimeException("Null or empty label : ${label}/${label?.replaceAll('\\p{Punct}', '').replaceAll(' +', '').trim()}");
    }

    if ( label.length() < 7 ) {  // Strings less than length 7
      base_string = label.replaceAll("\\p{Punct}", "").replaceAll(" +", "").trim().toUpperCase()
    }
    else if ( label.split(' ').length > 1 ) { // Strings with spaces between
      // Split the label upinto words and then collect the first letter of each word to create an acronym
      base_string = label.split(' ').collect{ it.length() > 0 ? it.substring(0,1) : ''}.join('').toUpperCase();
    }
    else {
      base_string = label.replaceAll("\\p{Punct}", "").replaceAll(" +", "").trim().substring(0,5).toUpperCase();
    }

    GrailsClass domain_class = grailsApplication.getArtefact("Domain",domain_class_name)
    int counter=1;

    boolean unique_slug_found = false;
    while ( ( counter < 50 ) && ( !unique_slug_found ) ) {
      result = base_string + ( counter > 1 ? "_${counter}" : '' );
      int num_matches = domain_class.clazz.executeQuery("select count(*) from ${domain_class_name} as d where d.${slugField} = :v".toString(), [v:result])[0] ;
      if ( num_matches == 0 ) {
        unique_slug_found = true;
      }
      else {
        counter++;
      }
    }

    return result;
  }
}
