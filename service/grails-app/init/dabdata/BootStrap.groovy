package dabdata

import grails.plugin.springsecurity.SpringSecurityUtils
import grails.plugin.springsecurity.SecurityFilterPosition

import groovy.util.logging.Slf4j

@Slf4j
class BootStrap {

  def grailsApplication
  def slugGeneratorService

  def init = { servletContext ->
    SpringSecurityUtils.clientRegisterFilter('oauthAuthenticationFilter', SecurityFilterPosition.PRE_AUTH_FILTER)

    log.info("dabdata (app ${grailsApplication.metadata.getApplicationVersion()} / grails ${grailsApplication.metadata.getGrailsVersion()}) initialising");
    log.info("              keycloak -> ${grailsApplication.config.keycloakUrl}");
    log.info("                 realm -> ${grailsApplication.config.keycloakRealm}");
    log.info("                   url -> ${grailsApplication.config.dataSource.url}");
    log.info("               dialect -> ${grailsApplication.config.dataSource.dialect}");
    log.info("                driver -> ${grailsApplication.config.dataSource.driverClassName}");
    log.info("          build number -> ${grailsApplication.metadata['build.number']}");
    log.info("          build branch -> ${grailsApplication.metadata['build.git.branch']}");
    log.info("          build commit -> ${grailsApplication.metadata['build.git.commit']}");
    log.info("            build time -> ${grailsApplication.metadata['build.time']}");
    log.info("              mailhost -> ${grailsApplication.config.mail.host}");
    log.info("         smtp username -> ${grailsApplication.config.mail.username}");
    log.info("              rr email -> ${grailsApplication.config.radioregister.senderaddr}");

    try{

      Party.withNewTransaction { status ->
        Party system = Party.findByShortcode('ROOT') 
        if ( system == null ) {
          system = new Party(shortcode:'ROOT', name:'System').save(flush:true, failOnError:true)
          log.info("Bootstrap creating new ROOT system party.... ID is ${system.id}");
        }
      }

      log.info("Checking refdata values");
      RefdataValue.withNewTransaction { status ->
        [
          [ catName:'ResourceStatus', label:'Proposed', value:'proposed' ],
          [ catName:'ResourceStatus', label:'Accepted', value:'accepted' ],
          [ catName:'ResourceStatus', label:'Rejected', value:'rejected' ],

          [ catName:'Region', label:'North East (England)', value:'northeasteng' ],
          [ catName:'Region', label:'North West (England)', value:'northwesteng' ],
          [ catName:'Region', label:'Yorkshire and The Humber', value:'yorks' ],
          [ catName:'Region', label:'East Midlands (England)', value:'eastmidseng' ],
          [ catName:'Region', label:'West Midlands (England)', value:'westmidseng' ],
          [ catName:'Region', label:'East of England', value:'easteng' ],
          [ catName:'Region', label:'London', value:'london' ],
          [ catName:'Region', label:'South East (England)', value:'southeasteng' ],
          [ catName:'Region', label:'South West (England)', value:'southwesteng' ],
          [ catName:'Region', label:'Scotland', value:'scotland' ],
          [ catName:'Region', label:'Wales', value:'wales' ],
          [ catName:'Region', label:'Northern Ireland', value:'ni' ],

          [ catName:'BitRate', label:'32kbps',  value:'32',  order:'000' ],
          [ catName:'BitRate', label:'40kbps',  value:'40',  order:'001' ],
          [ catName:'BitRate', label:'48kbps',  value:'48',  order:'002' ],
          [ catName:'BitRate', label:'56kbps',  value:'56',  order:'003' ],
          [ catName:'BitRate', label:'64kbps',  value:'64',  order:'004' ],
          [ catName:'BitRate', label:'80kbps',  value:'80',  order:'005' ],
          [ catName:'BitRate', label:'96kbps',  value:'96',  order:'006' ],
          [ catName:'BitRate', label:'112kbps', value:'112', order:'007' ],
          [ catName:'BitRate', label:'128kbps', value:'128', order:'008' ],
          [ catName:'BitRate', label:'160kbps', value:'160', order:'009' ],
          [ catName:'BitRate', label:'192kbps', value:'192', order:'010' ],
          [ catName:'BitRate', label:'224kbps', value:'224', order:'011' ],
          [ catName:'BitRate', label:'256kbps', value:'256', order:'012' ],
          [ catName:'BitRate', label:'320kbps', value:'320', order:'013' ],
          [ catName:'BitRate', label:'384kbps', value:'384', order:'014' ],

          [ catName:'SamplingRate', label:'32khz', value:'32' ],
          [ catName:'SamplingRate', label:'48khz', value:'48' ],

          [ catName:'DABLevel',    label:'DAB',    value:'DAB' ],
          [ catName:'DABLevel',    label:'DAB+',   value:'DAB+' ],

          [ catName:'ServiceMode', label:'Stereo', value:'Stereo' ],
          [ catName:'ServiceMode', label:'Mono',   value:'Mono' ],

          [ catName:'Protection', label:'EEP 1-A', value:'EEP1A'],
          [ catName:'Protection', label:'EEP 2-A', value:'EEP2A'],
          [ catName:'Protection', label:'EEP 3-A', value:'EEP3A', order:'0' ],
          [ catName:'Protection', label:'EEP 4-A', value:'EEP4A' ],
          [ catName:'Protection', label:'EEP 1-B', value:'EEP1B' ],
          [ catName:'Protection', label:'EEP 2-B', value:'EEP2B' ],
          [ catName:'Protection', label:'EEP 3-B', value:'EEP3B' ],
          [ catName:'Protection', label:'EEP 4-B', value:'EEP4B' ],
          [ catName:'Protection', label:'UEP 1',   value:'UEP1' ],
          [ catName:'Protection', label:'UEP 2',   value:'UEP2' ],
          [ catName:'Protection', label:'UEP 3',   value:'UEP3' ],
          [ catName:'Protection', label:'UEP 4',   value:'UEP4' ],
          [ catName:'Protection', label:'UEP 5',   value:'UEP5' ],
        ].each { rd ->
          def rv = RefdataValue.lookupOrCreate(rd.catName, rd.label, rd.value, rd.order)
        }
      }

      log.info("Validate transmitters slugs");
      Transmitter.withNewTransaction { status ->
        Transmitter.list().each { tr ->
          if ( tr.slug == null ) {
            log.info("Generate missing transmitter slug for ${tr.name}");
            tr.slug = slugGeneratorService.getSlug('dabdata.Transmitter', 'slug', tr.name);
            tr.save(flush:true, failOnError:true);
          }
        }
      }

      log.info("Validate broadcast service slugs");
      BroadcastService.withNewTransaction { status ->
        BroadcastService.list().each { bs ->
          if ( bs.slug == null ) {
            log.info("Generate missing broadcast service slug for ${bs.name}");
            bs.slug = slugGeneratorService.getSlug('dabdata.BroadcastService', 'slug', bs.name);
            bs.save(flush:true, failOnError:true);
          }
        }
      }

      log.info("Validate modification dates");
      BroadcastService.withNewTransaction { status ->
        BroadcastService.executeUpdate('update BroadcastService set dateCreated = :now where dateCreated is null',[now: new Date()]);
        BroadcastService.executeUpdate('update BroadcastService set lastUpdated = :now where lastUpdated is null',[now: new Date()]);
        BroadcastService.executeUpdate('update Transmitter set dateCreated = :now where dateCreated is null',[now: new Date()]);
        BroadcastService.executeUpdate('update Transmitter set lastUpdated = :now where lastUpdated is null',[now: new Date()]);
        BroadcastService.executeUpdate('update Multiplex set dateCreated = :now where dateCreated is null',[now: new Date()]);
        BroadcastService.executeUpdate('update Multiplex set lastUpdated = :now where lastUpdated is null',[now: new Date()]);
      }

    }
    catch ( Exception e ) {
      log.error("Exception processing data checks",e);
    }
  }

  def destroy = {
  }
}
