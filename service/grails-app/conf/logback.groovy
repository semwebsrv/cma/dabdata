import grails.util.BuildSettings
import grails.util.Environment
import org.springframework.boot.logging.logback.ColorConverter
import org.springframework.boot.logging.logback.WhitespaceThrowableProxyConverter
import java.nio.charset.StandardCharsets

import ch.qos.logback.core.rolling.RollingFileAppender
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy
import ch.qos.logback.core.util.FileSize


conversionRule 'clr', ColorConverter
conversionRule 'wex', WhitespaceThrowableProxyConverter

// logger ('grails.artefact.Interceptor', DEBUG)
// logger ('org.springframework.security.web.FilterChainProxy', DEBUG)

def targetDir = BuildSettings.TARGET_DIR

// See http://logback.qos.ch/manual/groovy.html for details on configuration
appender('STDOUT', ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        charset = StandardCharsets.UTF_8

        pattern =
                '%clr(%d{yyyy-MM-dd HH:mm:ss.SSS}){faint} ' + // Date
                        '%clr(%5p) ' + // Log level
                        '%clr(---){faint} %clr([%15.15t]){faint} ' + // Thread
                        '%clr(%-40.40logger{39}){cyan} %clr(:){faint} ' + // Logger
                        '%m%n%wex' // Message
    }
}


// In test we create a file appender to capture the SQL
if ( ( Environment.getCurrent()==Environment.TEST ) && ( targetDir != null) ) {

  appender("SQL_FILE", RollingFileAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%level %logger - %msg%n"
    }
    rollingPolicy(TimeBasedRollingPolicy) {
        fileNamePattern = "${targetDir}/sql-from-test-run-%d{yyyy-MM-dd_HH-mm}.log"
        maxHistory = 30
        totalSizeCap = FileSize.valueOf("2GB")
    }
  }

  logger ('grails.app.domains', DEBUG)
  logger ('grails.app.services', WARN)
  logger ('dabdata.oauth',WARN)
  logger ('dabdata', DEBUG)

  logger("org.hibernate.SQL", DEBUG, ["SQL_FILE"], false)
  logger("org.hibernate.type.descriptor.sql.BasicBinder", TRACE, ["SQL_FILE"], false)
  logger("gormfindbylqs", DEBUG);
}



if (Environment.isDevelopmentMode() && targetDir != null) {
    logger ('grails.app.domains', DEBUG)
    logger ('grails.app.services', WARN)
    logger ('dabdata', DEBUG)
    logger('gormfindbylqs', DEBUG);

    appender("FULL_STACKTRACE", FileAppender) {
        file = "${targetDir}/stacktrace.log"
        append = true
        encoder(PatternLayoutEncoder) {
            charset = StandardCharsets.UTF_8
            pattern = "%level %logger - %msg%n"
        }
    }
    logger("StackTrace", ERROR, ['FULL_STACKTRACE'], false)
}

logger("org.hibernate.orm.deprecation", ERROR);
root(INFO, ['STDOUT'])
