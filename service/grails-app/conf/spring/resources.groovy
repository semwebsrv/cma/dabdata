import dabdata.oauth.*;
import dabdata.*;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.http.HttpStatus;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import grails.util.Environment
import io.swagger.models.Swagger
import io.swagger.models.Info
import dabdata.DDAuditRequestResolver

beans = {

  // Change the authentication end point to throw a 401
  authenticationEntryPoint(HttpStatusEntryPoint, HttpStatus.UNAUTHORIZED)

  // Referenced from plugins/springsecurity in application.yaml
  oauthAuthenticationProvider(OAuthAuthenticationProvider)

  accessDeniedHandler(OAuthAwareAccessDeniedHandler)

  switch(Environment.current) {
    case Environment.TEST:
      certificateManager(MockCertificateManagerImpl)
      break
    default:
      certificateManager(KeycloakCertificateManagerImpl)
      break
  }

  // This filter registers itself in a particular order in the chain.
  // N.B. See Bootstrap.init for the other half of this setup
  oauthAuthenticationFilter(OAuthAuthenticationFilter){ bean ->
    authenticationManager = ref('authenticationManager')
  }

  // Because of the custom ordering above we need to stop this from being de/registered automatically.
  oauthAuthenticationFilterDeregistrationBean(FilterRegistrationBean) {
    filter = ref('oauthAuthenticationFilter')
    enabled = false
  }

  apiInfo(Info) {
    title="DAB Service Registry API"
    description="This API Provides access to the DBA service provider registry."
    termsOfService="tbc"
    version="${grailsApplication.metadata.getApplicationVersion()}"
  }

  swagger(Swagger) {
    // see https://github.com/OAI/OpenAPI-Specification/blob/main/versions/2.0.md#schema
    // securityDefinitions = ["apiKey": new ApiKeyAuthDefinition("apiKey", In.HEADER)]
    // security = [new SecurityRequirement().requirement("apiKey")]
    info = apiInfo
  }


  // Some prompting taken from
  // https://stackoverflow.com/questions/69903035/grails-4-audit-logging-w-spring-security-does-not-record-actor
  auditRequestResolver(DDAuditRequestResolver) {
  }
}
