

# 2021-02-26 - 0.0.2-SNAPSHOT Open for changes

- Add gson template for bearer with conditional rendering for service and transmitter
- Add bearers and transmitters custom gson rendering for multiplex to prevent recursive rendering
- Add owner property to Transmitter, Multiplex and BroadcastService
- Add gson template to set out multiplex details - adds list of bearers and transmitters to basic Json record
- Add properties to model an authority permissio file for each Party - part of a distributed and decentralized authenticaiton and authorization model for the more general directory app
- Implement element set name (setname) parameter for multiplex mapping, setting the general pattern for search results vs full record retrieval
