#!/bin/bash

if [ ! -f env.sh ] 
then
  echo Please copy template_env.sh to env.sh and set the password accordingly
  exit 1
fi

source ./env.sh

# N.B. In the default keycloak realm setup - the admin user has the clusteradmin role


# -H 'accept: application/json' -H 'Content-type: application/x-www-form-urlencoded' \
LOGIN_RESPONSE=$(curl -s -X POST -H 'Content-type: application/x-www-form-urlencoded' \
        --data-urlencode "client_id=rrclient" \
        --data-urlencode "username=$USER" \
        --data-urlencode "password=$PASS" \
        --data-urlencode "grant_type=password" \
        "${KEYCLOAK_BASE_URL}/auth/realms/RadioRegistry/protocol/openid-connect/token")

JWT=`echo $LOGIN_RESPONSE | jq -rc '.access_token'`
echo Auth token will be $JWT


# printf "\n\nRequesting status from $MTCPADMIN_BASE_URL \n"
#STATUS=`curl -s -H "Accept: application/json" -H "Authorization: bearer $TENANT_ADMIN_JWT" -H "X-TENANT: vanillatest" "$HOLO_API_TEST_BASE_URL/holoCoreService/test"`
#echo status result: $STATUS
#echo $STATUS | jq

# Call the rest endpoint to search multiplexes
MULTIPLEX_Q1=`curl -s -H "Accept: application/json" -H "Authorization: bearer $JWT" "$RR_BASE_URL/dabdata/multiplex?q=name:test"`
echo Result of multiplex query 1: $MULTIPLEX_Q1


# Post a new multiplex to the rest endpoint
curl -H "Accept: application/json" \
     -H "Authorization: bearer $JWT" \
     -H "Content-Type: application/json" \
     -X POST "$RR_BASE_URL/dabdata/multiplex" -d '
{
  "name":"Test Multiplex 1"
}
'

# Rerun the query a second time - should find our new multiplex
MULTIPLEX_Q2=`curl -s -H "Accept: application/json" -H "Authorization: bearer $JWT" "$RR_BASE_URL/dabdata/multiplex?q=name:test"`
echo Result of multiplex query 1: $MULTIPLEX_Q2
