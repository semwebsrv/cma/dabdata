

create extension pg_trgm;
CREATE INDEX multiplex_name_trigram_idx ON multiplex USING GIN (mp_name gin_trgm_ops);
CREATE INDEX multiplex_area_trigram_idx ON multiplex USING GIN (mp_area gin_trgm_ops);

CREATE INDEX transmitter_name_trigram_idx ON transmitter USING GIN (tr_name gin_trgm_ops);
