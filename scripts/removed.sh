# Explore graphql interface

# Graphql Introspection
curl "$RR_BASE_URL/dabdata/graphql" \
       -H "Authorization: Bearer $JWT" \
       -H "Accept: application/json" \
       -H "Content-Type: application/json" -d '
{
  "query": "query { __schema { types { name fields { name } }, queryType { name }, mutationType { name }  } }",
  "variables":{
  }
}' | jq


# Graphql Create
curl "$RR_BASE_URL/dabdata/graphql" \
       -H "Authorization: Bearer $JWT" \
       -H "Accept: application/json" \
       -H "Content-Type: application/json" -d '
{
  "query": "mutation($multiplex: MultiplexCreate) { multiplexCreate(multiplex: $multiplex) { id name } }",
  "variables":{
    "multiplex": {
      "name": "Test Multiplex 2"
    }
  }
}'

echo Test graphql query

# Graphql Query
curl "$RR_BASE_URL/dabdata/graphql" \
       -H "Authorization: Bearer $JWT" \
       -H "Accept: application/json" \
       -H "Content-Type: application/json" -d '{
  "query": "query { findMultiplexByLuceneQueryString(q:"test") {  totalCount results { name } } }",
  "variables":{
  }
}'

