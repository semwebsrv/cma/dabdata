# DABData


This project is a microservice that supports a registry of radio services - it provides a number of resource endpoints relating to the major entities
 
* Multiplexes
* Bearers
* Broadcast Services
* Organisations
* Transmitters

All service endpoints are secured with a JWT passed by a bearer token in the authentication heder. Un UAT the token is obtained from a keycloak instance. The service
is the partner half to the front end react bundle at https://gitlab.com/semwebsrv/cma/dabregistry


Props and Kudos to the folks over at radiodns for all their smart work on the entity model for all this - trying to make sense of the raw OFCOM data without standing on their shoulders would have been a royal PITA.

see: https://raw.githubusercontent.com/njh/radiodns-uk/master/docs/data-model.png

# notes

https://stackoverflow.com/questions/61500578/how-to-mock-jwt-authenticaiton-in-a-spring-boot-unit-test

https://guides.grails.org/grails-oauth-google/guide/index.html

CREATE USER xyz WITH PASSWORD 'xyz';

CREATE DATABASE xyz;

GRANT ALL PRIVILEGES ON DATABASE xyz to xyz;

# See also

https://github.com/steamcleaner/swagger-grails
https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Gradle.gitlab-ci.yml


# Deployment

## Docker

The dockerfile understands the following environment variables
| variable | for | default |
| --- | --- | --- |
| SMTP_USERNAME | username for logging on to SMTP server | |
| SMTP_PASSWORD | password for logging on to SMTP server | |
| DATASOURCE_USERNAME |||
| DATASOURCE_PASSWORD |||
| DATASOURCE_URL |||
| RADIOREGISTER_CONTACT_ADDR | Comma separated list of email addresses who will receive system notifications ||
| RADIOREGISTER_SENDER_ADDR | Email address to use for outgoing mail messages ||


## K8S

The example k8s config files in the k8s dir expects a dabdata secret blocks to complete it's configuration in the local namespace 
which provides the properties above

# Bootstrapping

You most likely want to bootstrap RR with data from the OFCOM in one form or another. It is also likely that you wish to cross reference
the OFCOM data with RadioDNS who's station data is substantially richer.

It is suggested you start with the OFCOM TxParams data as the "Authoritative" source. All DAB services listed in the DAB TXParams sheet have an 
allocated SID which means we can use the unique constraint on SID to control the data to some extent.

## Bootstrapping OFCOM TXParams

The agent at https://gitlab.com/semwebsrv/cma/dabdataagents/-/tree/main/txparams uses the internal loadDataFromUrl endpoint to force a new installation
to synchronize with the latest OFCOM data. RR has been changed to only read in DAB multiplex data from this file, tho that restriction could be lifted by
editing ETLHelperService and removing the restriction which requires the multiplex license to start SSDAB.

## Bootstrapping and Enriching data with RadioDNS

the agent at https://gitlab.com/semwebsrv/cma/dabdataagents/-/tree/main/radiodns deals with parsing RadioDNS entries and submitting them
to the update endpoint.

## Bootstrapping the Community Radio Station Data

Adding Community station, for stations not yet broadcasting using DAB, is a double edged sword. It's nice to see community stations in the database
but this is a DAB database and the primary key for broadcast services here is the SID - Stations not yet allocated a SID can be added but......
The agent at https://gitlab.com/semwebsrv/cma/dabdataagents/-/tree/main/community_stations can be used to upload and sync with the community stations list
at http://static.ofcom.org.uk/static/radiolicensing/html/radio-stations/community/community-main.htm.

An example of this problem is "Huntingdon Community Radio" which is in DAB TXParams with SID CA8A. This station also appears in the list of 
community stations but with the name "HCR104fm". By cross-referencing the SID with RadioDNS we are able to find a better name, but the key to this
correlation is the SID. Community stations without a SID could not be so matched. Loading the OFCOM list of community stations currently would result
in duplicate records for Huntingdon - one for the CR list, and one generated from DAB TXParams/RadioDNS.

Loading the list of community stations without a safety net could well result in multiple records for single broadcast services. Unfortunately, because
the broadcast service license is not present in DAB TXParams there is no good way to cross-reference this data.





NOTES

From: https://onlinelibrary.wiley.com/doi/pdf/10.1002/0470871431.app1

1.2  Important Relations
1CU = 64 bits = 8 bytes
1 CIF = 864 CU = 55.296 kbits = 6.912 kbytes
1 FIB = 256 bits = 32 bytes
Where: CU=Capacity Unit

standard: https://www.etsi.org/deliver/etsi_tr/101400_101499/10149601/01.01.01_60/tr_10149601v010101p.pdf

Mean code rates (page 10) suggests sampling rates of 24,32,64,128,192,224 and 256

CU Calculator: https://www.opendigitalradio.org/software/capacity-units-calculator
               https://opendigitalradio.github.io/cu_calc/


